// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAvT45Kdm_oXKWCK_ux0ZsPIOzMOYvNAdU",
    authDomain: "femeka-f27a3.firebaseapp.com",
    projectId: "femeka-f27a3",
    storageBucket: "femeka-f27a3.appspot.com",
    messagingSenderId: "360503754726",
    appId: "1:360503754726:web:139b5e5383767f9dd4a027",
    measurementId: "G-XQBDHY316P"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
