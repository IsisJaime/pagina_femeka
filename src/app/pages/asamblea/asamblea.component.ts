import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-asamblea',
  templateUrl: './asamblea.component.html',
  styleUrls: ['./asamblea.component.scss']
})
export class AsambleaComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    if (!localStorage.getItem('foo')) { 
      localStorage.setItem('foo', 'no reload') 
      location.reload() 
    } else {
      localStorage.removeItem('foo') 
    }
  }

  loadScripts() {
    const dynamicScripts = [
     'https://www.gstatic.com/charts/loader.js',
     'https://maps.googleapis.com/maps/api/js?key=undefined',
     'https://www.google.com/jsapi',
     './assets/js/geochart.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

}
