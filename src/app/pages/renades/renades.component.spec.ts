import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RenadesComponent } from './renades.component';

describe('RenadesComponent', () => {
  let component: RenadesComponent;
  let fixture: ComponentFixture<RenadesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RenadesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RenadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
