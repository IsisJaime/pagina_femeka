import { Component, OnInit } from '@angular/core';
import { Presidentes } from '../../services/presidentes/presidentes.service';
import { renades } from '../../interfaces/renade';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-renades',
  templateUrl: './renades.component.html',
  styleUrls: ['./renades.component.scss']
})
export class RenadesComponent implements OnInit {

  public curp='';
  public registro:renades = {} as renades;
  public tabla = false

  constructor(private _renade:Presidentes) { }

  ngOnInit(): void {
  }

  buscarRenade(){
    if (this.curp.length > 0) {
      this._renade.findRenade(this.curp.toUpperCase()).subscribe((resp:any)=>{
        if (resp.ok) {
          if (resp['respuesta'].length > 0) {
            this.registro = resp['respuesta'][0]
            this.tabla = true
          }else{
            Swal.fire('Aun no cuentas con registro de renade', '', 'error')
            this.tabla = false
            this.registro = {} as renades;
            this.curp = '';
          }
        }
      })
    }else{
      this.curp = '';
      Swal.fire('Es necesario ingresar la curp', '', 'error')
    }
  }

}
