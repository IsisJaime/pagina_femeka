import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarRenadeComponent } from './actualizar-renade.component';

describe('ActualizarRenadeComponent', () => {
  let component: ActualizarRenadeComponent;
  let fixture: ComponentFixture<ActualizarRenadeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActualizarRenadeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarRenadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
