import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RenadesService } from '../../services/renades/renades.service';
import { renades } from '../../interfaces/renade';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-actualizar-renade',
  templateUrl: './actualizar-renade.component.html',
  styleUrls: ['./actualizar-renade.component.scss']
})
export class ActualizarRenadeComponent implements OnInit {

  public id:any;
  public renade:renades = {} as renades;

  constructor(private _route:ActivatedRoute,
              private router: Router,
              private _renade:RenadesService) { }

  ngOnInit(): void {
    this.id = this._route.snapshot.paramMap.get('id');
    this.obtenerRenade(this.id);
  }

  obtenerRenade(id:any){
    this._renade.getRenadeId(id).subscribe((resp:any)=>{
      if (resp.ok) {
        this.renade = resp['respuesta'];
      }
    })
  }

  updateRenade(form:NgForm){
    this._renade.updateRenade(this.id,form.form.value).subscribe((resp:any)=>{
      if (resp.ok) {
        Swal.fire('Se actualizo el renade', '', 'error')
        this.router.navigate(['/dashboard/renades/estado']);
      }
    })
  }

}
