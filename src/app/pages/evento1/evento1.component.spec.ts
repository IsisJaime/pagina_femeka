import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Evento1Component } from './evento1.component';

describe('Evento1Component', () => {
  let component: Evento1Component;
  let fixture: ComponentFixture<Evento1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Evento1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Evento1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
