import { Component, OnInit } from '@angular/core';
import * as AOS from 'aos';
@Component({
  selector: 'app-evento1',
  templateUrl: './evento1.component.html',
  styleUrls: ['./evento1.component.scss']
})
export class Evento1Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
    AOS.init();
  }

}
