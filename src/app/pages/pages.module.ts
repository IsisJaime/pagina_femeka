import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router';
import { ComponentsModule } from '../components/components.module';
import { BrowserModule } from '@angular/platform-browser';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PagesComponent } from './pages.component';
import { SharedModule } from '../shared/shared.module';
import { TotalesComponent } from './totales/totales/totales.component';
import { AdminComponent } from './administrador/admin/admin.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { AboutComponent } from './about/about.component';
import { VisionMisionComponent } from './vision-mision/vision-mision.component';
import { OrganigramaComponent } from './organigrama/organigrama.component';
import { EventosComponent } from './eventos/eventos.component';
import { AsambleaComponent } from './asamblea/asamblea.component';
import { DetallesEventosComponent } from './detalles-eventos/detalles-eventos.component';
import { NoticiasComponent } from './noticias/noticias.component';
import { SeminariosComponent } from './seminarios/seminarios.component';
import { Evento1Component } from './evento1/evento1.component';
import { Evento2Component } from './evento2/evento2.component';
import { Evento3Component } from './evento3/evento3.component';
import { Seminario1Component } from './seminario1/seminario1.component';
import { RenadesComponent } from './renades/renades.component';
import { ActivarRenadesComponent } from './activar-renades/activar-renades.component';
import { SuperAdminComponent } from '../pages/super-admin/super-admin.component';
import { RenadesEstadoComponent } from './renades-estado/renades-estado.component';
import { ActualizarRenadeComponent } from './actualizar-renade/actualizar-renade.component';
import { RegistroEventsComponent } from './registro-events/registro-events.component';

@NgModule({
  declarations: [
    DashboardComponent,
    PagesComponent,
    TotalesComponent,
    AdminComponent,
    AboutComponent,
    VisionMisionComponent,
    OrganigramaComponent,
    EventosComponent,
    AsambleaComponent,
    DetallesEventosComponent,
    NoticiasComponent,
    SeminariosComponent,
    Evento1Component,
    Evento2Component,
    Evento3Component,
    Seminario1Component,
    RenadesComponent,
    ActivarRenadesComponent,
    SuperAdminComponent,
    RenadesEstadoComponent,
    ActualizarRenadeComponent,
    RegistroEventsComponent

  ],
  exports: [
    DashboardComponent,
    PagesComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule,
    ComponentsModule,
    BrowserModule,
    NgxDropzoneModule
  ]
})
export class PagesModule { }
