import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Evento2Component } from './evento2.component';

describe('Evento2Component', () => {
  let component: Evento2Component;
  let fixture: ComponentFixture<Evento2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Evento2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Evento2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
