import { Component, OnInit } from '@angular/core';
import * as AOS from 'aos';
@Component({
  selector: 'app-evento2',
  templateUrl: './evento2.component.html',
  styleUrls: ['./evento2.component.scss']
})
export class Evento2Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
    AOS.init();
  }

}
