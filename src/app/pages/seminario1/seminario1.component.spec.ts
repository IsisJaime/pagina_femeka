import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Seminario1Component } from './seminario1.component';

describe('Seminario1Component', () => {
  let component: Seminario1Component;
  let fixture: ComponentFixture<Seminario1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Seminario1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Seminario1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
