import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivarRenadesComponent } from './activar-renades.component';

describe('ActivarRenadesComponent', () => {
  let component: ActivarRenadesComponent;
  let fixture: ComponentFixture<ActivarRenadesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActivarRenadesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivarRenadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
