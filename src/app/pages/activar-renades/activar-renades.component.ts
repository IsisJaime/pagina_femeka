import { Component, OnInit } from '@angular/core';
import { Presidentes } from '../../services/presidentes/presidentes.service';


@Component({
  selector: 'app-activar-renades',
  templateUrl: './activar-renades.component.html',
  styleUrls: ['./activar-renades.component.scss']
})
export class ActivarRenadesComponent implements OnInit {

  public presidentes:any[] = [];

  constructor(private _presidente:Presidentes) { }

  ngOnInit(): void {
    this.obtenerPresidentes();
  }

  obtenerPresidentes(){
    this._presidente.obtenerPresidentes().subscribe((resp:any)=>{
      if (resp.ok) {
        resp['respuesta'].forEach((element:any) => {
          if(element.role == "presidente"){
            if(element.estado != 'Jonacatepec'){
              this.presidentes.push(element);
            }
          }
        });
      }
    })
  }

  actualizarPresi(evento:any, item:any){
    let datos = {
      renades:item.renades + parseInt(evento.target.value)
    }
    this._presidente.actualizarRenades(item._id,datos).subscribe((resp:any)=>{
      let encontar = this.presidentes.findIndex(element => element._id == resp['respuesta']._id)
      this.presidentes[encontar] = resp['respuesta']
    })
  }

}
