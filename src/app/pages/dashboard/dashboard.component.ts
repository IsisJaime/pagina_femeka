import { Component, OnInit } from '@angular/core';
import { RegistrosService } from '../../services/registros/registros.service';
import { Router } from '@angular/router';
import * as AOS from 'aos';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';

import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public atleta = [];
  public entrenador = [];
  public examinador = [];
  public jueces = [];
  public voucher = [];
  public president: any;
  public nombre: any;
  public id: any;
  public idPresidente: any;
  public equipo = [];
  urlImage?: Observable<string>;

  constructor(public _registro: RegistrosService,
              private router: Router,
              private _storage: AngularFireStorage,
              private spinner: NgxSpinnerService) { }


  ngOnInit(): void {
    this.obtenerPresidente();
    AOS.init();

    this.spinner.show();

    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 5000);
  }

  obtenerPresidente() {
    this.president = localStorage.getItem('usuario');
    this.nombre = this.president.replace(/['"]+/g, '');
    this.id = localStorage.getItem('id')
    this.idPresidente = this.id.replace(/['"]+/g, '');
  }

  /* files: File[] = [];

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
  }

  onRemove(event: any) {
    this.files.splice(this.files.indexOf(event), 1);
  }

  atletass(event: any) {
    console.log(event);

    this.atleta = event;
  }

  equipoatlet(event: any) {
    this.equipo = event
  }

  entrenadores(event: any) {
    this.entrenador = event;
  }

  examinadores(event: any) {
    this.examinador = event;
  }

  juez(event: any) {
    this.jueces = event;
  }

  subirImagen(e: any) {
    const randomId = Math.random().toString(36).substring(2);
    const file = e;
    const filePath = `vouchers/${randomId}`;
    const ref = this._storage.ref(filePath);
    const task = this._storage.upload(filePath, file);
    task.snapshotChanges().pipe(
      finalize(() => {
        ref.getDownloadURL().subscribe(urlImage => {
          this.urlImage = urlImage;
          this.cargarImagen(urlImage);
        })
      })
    ).subscribe();
  }

  cargarImagen(url: any) {
    let data = {
      bouchers: url,
      presidente: this.idPresidente
    }
    this._registro.agregarImg(data).subscribe((resp: any) => {
      if (resp.ok) {
        console.log(resp);
      }
    })
  }

  enviar() {
    Swal.fire({
      title: '¿Deseas guardar tus datos?',
      showDenyButton: true,
      confirmButtonText: `Guardar`,
      denyButtonText: `No Guardar`,
    }).then((result) => {
      if (result.isConfirmed) {
        const formData = new FormData();
          if (this.atleta.length != 0) {
            this.atleta.forEach(element => {
              let atlee = Object.assign(element, { presidente: this.nombre })
              this._registro.agregarAtle(atlee).subscribe((resp: any) => {
                if (resp.ok) {
                  console.log(resp);
                }
              })
            })
          }

          if (this.equipo.length != 0) {
            this.equipo.forEach((element: any) => {
              element.atletas.forEach((elements: any) => {
                let atlee = Object.assign(elements, { presidente: this.nombre })

                this._registro.agregarAtle(atlee).subscribe((resp: any) => {
                  if (resp.ok) {
                    console.log(resp);
                  }
                })
              });
            })
          }


          this.examinador.forEach(element => {
            let exami = Object.assign(element, { presidente: this.nombre })
            this._registro.agregarExami(exami).subscribe((resp: any) => {
              if (resp.ok) {
                console.log(resp);
              }
            })
          })


          this.entrenador.forEach(element => {
            let entre = Object.assign(element, { presidente: this.nombre })
            this._registro.agregarEntre(entre).subscribe((resp: any) => {
              if (resp.ok) {
                console.log(resp);

              }
            })
          })

          this.jueces.forEach(element => {
            let juez = Object.assign(element, { presidente: this.nombre })
            this._registro.agregarJuecess(juez).subscribe((resp: any) => {
              if (resp.ok) {
                console.log(resp);

              }
            })
          })

          this.files.forEach((element) => {
            this.subirImagen(element);
          })

          Swal.fire({
            title: 'Datos Guardados Correctamente',
            icon: 'success',
            showDenyButton: false,
            confirmButtonText: `Guardar`,
          }).then((result) => {
            this.router.navigate(['/login'])
          })
      } else if (result.isDenied) {
        Swal.fire('No se guardaron tus datos', '', 'error')
      }
    })
  } */

  cerrar() {
    localStorage.clear();
    this.router.navigate(['/']);

    /* //RECARGAR PAGINA HOME
    var landingUrl = "/";
    window.location.href = landingUrl; */

  }
}
