import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegistrosService } from '../../../services/registros/registros.service';

@Component({
  selector: 'app-totales',
  templateUrl: './totales.component.html',
  styleUrls: ['./totales.component.scss']
})
export class TotalesComponent implements OnInit {

  public totalAtleta = {
    respuesta: []
  }

  public totalJuces = {
    respuesta: []
  }

  public totalEntrenadores = {
    respuesta: []
  }

  public totalExaminadores = {
    respuesta: []
  }

  public contadorJueces = 0
  public contadorExaminadores = 0
  public contadorEntrenadores = 0

  public estadoSeleccionado = ""

  //Atleta
  public atleta: any
  public atletaEstado = []
  public atletasEstado: any = []

  //Juez
  public juez: any
  public juezEstado: any = []
  public juecesEstados: any = []

  //Entrenador
  public entrenadores: any
  public entrenadorEstado: any = []
  public entrenadoresEstados: any = []

  //Examinadores
  public examinadores: any
  public examinadorEstado: any = []
  public examinadoresEstados: any = []

  public imagenUrl:any;
  public img:any=[];
  public url = 'https://femak-back.herokuapp.com/ver/img/';
  public habilitar = false;

  constructor(public _registro: RegistrosService, public _route: Router) { }

  ngOnInit(): void {
    this.allAtletas();
    this.allJuces();
    this.allEntrenadores();
    this.allExaminadores();
  }

  allAtletas() {
    this._registro.obtenerAtletas().subscribe((resp: any) => {
      this.totalAtleta = resp.respuesta.length
      this.atletaEstado = resp.respuesta
    })
  }

  allJuces() {
    this._registro.obtenerJueces().subscribe((data: any) => {
      this.totalJuces = data.respuesta.length;
      this.juezEstado = data.respuesta
    })
  }

  allEntrenadores() {
    this._registro.obtenerEntrenadores().subscribe((data: any) => {
      this.totalEntrenadores = data.respuesta.length;
      this.entrenadorEstado = data.respuesta;
    })
  }

  allExaminadores() {
    this._registro.obtenerExaminadores().subscribe((data: any) => {
      this.totalExaminadores = data.respuesta.length;
      this.examinadorEstado = data.respuesta
    })
  }

  estado() {
    this.estadoAtletas();
    this.estadoJueces();
    this.estadoEntrenadores();
    this.estadoExaminadores();
  }

  estadoAtletas(){
    this.atletasEstado = []
    this.atletaEstado.forEach((element: any) => {
      if (element.estado != null) {
        if (element.estado == this.estadoSeleccionado) {
          this.atletasEstado.push(element)
        }
      }
    })
  }

  estadoJueces() {
    this.juecesEstados = []
    this.juezEstado.forEach((element: any) => {
      if (element.estado != null) {
        if (element.estado == this.estadoSeleccionado) {
          this.juecesEstados.push(element)
        }
      }
    })
  }

  estadoEntrenadores() {
    this.entrenadoresEstados = []
    this.entrenadorEstado.forEach((element: any) => {
      if (element.estado != null) {
        if (element.estado == this.estadoSeleccionado) {
          this.entrenadoresEstados.push(element)
        }
      }
    })
  }

  estadoExaminadores() {
    this.examinadoresEstados = []
    this.examinadorEstado.forEach((element: any) => {
      if (element.estado != null) {
        if (element.estado == this.estadoSeleccionado) {
          this.examinadoresEstados.push(element)
        }
      }
    })
  }

  descargarVoucher() {
    this._registro.obtenerIdPresidente(this.estadoSeleccionado).subscribe((resp:any) =>{
      if(resp.ok){
        resp.respuesta.forEach((element:any) => {
          if(element.role == 'presidente'){
            this._registro.obtenerUrlImg(element._id).subscribe((resp:any) =>{
              if(resp.ok){
                this.setImageners(resp.respuesta);
              }
            })
          }
        });
      }
    })
  }

  setImageners(img:any){
    img.forEach((element:any) => {
      this.img.push(element.bouchers)
    });
    this.habilitar = true;
    this.descarga(this.img);
  }

  descarga(img:any){
    img.forEach((element:any) => {

    });
  }
  /* descargarVoucher() {
    this._registro.obtenerIdPresidente(this.estadoSeleccionado).subscribe((resp:any) =>{
      if(resp.ok){
        this._registro.obtenerUrlImg(resp.respuesta[0]._id).subscribe((resp:any) =>{
          console.log(resp.respuesta[0]._id," este es el id del presidente")
          if(resp.ok){
            console.log(resp);

            this.setImageners(resp.respuesta);
          }
        })
      }
    })

  }

  // metodo atletas

  setImageners(img:any){
    img.forEach((element:any) => {
      let cadena = [];
      cadena = element.bouchers.split('/');
      this.img.push(this.url.concat(cadena[3]))
      cadena = [];

    });
    console.log(this.img,"asi manda la ruta");
    this.habilitar = true;
  }
  allAtletas() {
    this._registro.obtenerAtletas().subscribe((resp: any) => {
      this.totalAtleta = resp
      this.atleta = this.totalAtleta.respuesta.length;
      this.setAtletasEstado(resp.respuesta)
    })
  }

  setAtletasEstado(atleta: any) {
    this.atletaEstado = atleta
  }

  estado() {
    this.atletasEstado = []
    this.atletaEstado.forEach((element: any) => {
      if (element.estado != null) {
        if (element.estado == this.estadoSeleccionado) {
          this.atletasEstado.push(element)
        }
      }
    })
    this.estadoJueces();
    this.estadoEntrenadores();
    this.estadoExaminadores();
  }
  //metodo juces

  allJuces() {
    this._registro.obtenerJueces().subscribe((data: any) => {
      this.totalJuces = data;
      this.juez = this.totalAtleta.respuesta.length;
      this.setJuecesEstado(data.respuesta)
    })
  }

  setJuecesEstado(juez: any) {
    this.juecesEstados = juez
  }

  estadoJueces() {
    this.juecesEstados = []
    this.juezEstado.forEach((element: any) => {
      console.log(element);

      element.camposJueces.forEach((data2: any) => {
        if (data2.estado == this.estadoSeleccionado) {
          this.juecesEstados.push(data2)

        }
      });

    })
  }

  //metodo entrenadores

  allEntrenadores() {
    this._registro.obtenerEntrenadores().subscribe((data: any) => {
      console.log(data);
      this.totalEntrenadores = data;

      this.setEntrenadoresEstado(data.respuesta)
    })
  }

  setEntrenadoresEstado(entrenador: any) {

    this.entrenadores = entrenador.length

  }

  estadoEntrenadores() {
    this.entrenadoresEstados = []
    this.entrenadorEstado.forEach((element: any) => {
      element.campos.forEach((data2: any) => {
        if (data2.estado == this.estadoSeleccionado) {
          this.entrenadoresEstados.push(data2)

        }
      });

    })
    console.log(this.entrenadoresEstados);

  }

  //metodo examinadores

  allExaminadores() {
    this._registro.obtenerExaminadores().subscribe((data: any) => {
      console.log(data);
      this.totalExaminadores = data;

      this.setExaminadores(data.respuesta)
    })
  }

  setExaminadores(examinador: any) {

    this.examinadores = examinador.length

  }

  estadoExaminadores() {
    this.examinadoresEstados = []
    this.examinadorEstado.forEach((element: any) => {
      element.camposExaminadores.forEach((data2: any) => {
        if (data2.estado == this.estadoSeleccionado) {
          this.examinadoresEstados.push(data2)

        }
      });

    })
    console.log(this.examinadoresEstados);

  } */

}
