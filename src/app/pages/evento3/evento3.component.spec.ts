import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Evento3Component } from './evento3.component';

describe('Evento3Component', () => {
  let component: Evento3Component;
  let fixture: ComponentFixture<Evento3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Evento3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Evento3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
