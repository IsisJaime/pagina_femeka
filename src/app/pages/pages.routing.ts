import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import{ AdminComponent } from './administrador/admin/admin.component';
import{ TotalesComponent } from './totales/totales/totales.component';
import{ AboutComponent } from './about/about.component';
import { VisionMisionComponent } from './vision-mision/vision-mision.component';
import { OrganigramaComponent } from './organigrama/organigrama.component';
import { EventosComponent } from './eventos/eventos.component';
import { AsambleaComponent } from './asamblea/asamblea.component';
import { DetallesEventosComponent } from './detalles-eventos/detalles-eventos.component';
import { NoticiasComponent } from './noticias/noticias.component';
import { SeminariosComponent } from './seminarios/seminarios.component';
import { Evento1Component } from './evento1/evento1.component';
import { Evento2Component } from './evento2/evento2.component';
import { Evento3Component } from './evento3/evento3.component';
import { Seminario1Component } from './seminario1/seminario1.component';
import { RenadesComponent } from './renades/renades.component';
import { ActivarRenadesComponent } from './activar-renades/activar-renades.component';
import { SuperAdminComponent } from './super-admin/super-admin.component';
import { RenadesEstadoComponent } from './renades-estado/renades-estado.component';
import { ActualizarRenadeComponent } from './actualizar-renade/actualizar-renade.component';
import { RegistroEventsComponent } from './registro-events/registro-events.component';

const routes: Routes = [
    {
        path: 'dashboard',
        component: PagesComponent,
        children: [
            { path: '', component: DashboardComponent },
            { path:'admin' , component: AdminComponent},
            { path:'totales' , component: TotalesComponent},
            { path:'about' , component: AboutComponent},
            { path:'vision-mision' , component: VisionMisionComponent},
            { path:'organigrama' , component: OrganigramaComponent},
            { path:'eventos' , component: EventosComponent},
            { path:'asamblea' , component: AsambleaComponent},
            { path:'detalles-eventos' , component: DetallesEventosComponent},
            { path:'noticias' , component: NoticiasComponent},
            { path:'seminarios' , component: SeminariosComponent},
            { path:'evento1' , component: Evento1Component},
            { path:'evento2' , component: Evento2Component},
            { path:'evento3' , component: Evento3Component},
            { path:'seminario1' , component: Seminario1Component},
            { path:'renades', component:RenadesComponent },
            { path:'activar/renades', component:ActivarRenadesComponent },
            { path:'super/admin', component:SuperAdminComponent },
            { path:'renades/estado', component:RenadesEstadoComponent },
            { path:'actualizar/renade/:id', component:ActualizarRenadeComponent },
            { path:'registro/events', component:RegistroEventsComponent }
        ]
    },
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class PagesRoutingModule {}
