import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroEventsComponent } from './registro-events.component';

describe('RegistroEventsComponent', () => {
  let component: RegistroEventsComponent;
  let fixture: ComponentFixture<RegistroEventsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistroEventsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
