import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
import { RegistrosService } from 'src/app/services/registros/registros.service';
import { finalize } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { ConceptosService } from '../../services/conceptos-pagos/conceptos.service';

@Component({
  selector: 'app-registro-events',
  templateUrl: './registro-events.component.html',
  styleUrls: ['./registro-events.component.scss']
})
export class RegistroEventsComponent implements OnInit {

  public atleta = [];
  public entrenador = [];
  public examinador = [];
  public jueces = [];
  public voucher = [];
  public president: any;
  public nombre: any;
  public id: any;
  public idPresidente: any;
  public equipo = [];
  urlImage?: Observable<string>;
  public conceptosDePagos:any[] = [];
  public conceptoSeleccionado = '';
  public precioConcepto = 0;
  public cantidad = 0;
  public desglose:any[] = [];
  public total = 0;
  public concep = '';

  constructor(public _registro: RegistrosService,
              private router: Router,
              private _storage: AngularFireStorage,
              private _conceptos:ConceptosService,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.obtenerPresidente();
    this.obtenerConceptos();
  }

  obtenerConceptos(){
    this.conceptosDePagos = this._conceptos.getConceptos();
  }

  agregarConcepto(){
    let concepto = this.conceptosDePagos.find(element => element.concepto == this.concep)
    this.conceptoSeleccionado = concepto.concepto
    this.precioConcepto = concepto.costo
  }

  generarConcepto(){
    let total = 0;
    let datos = {
      concepto:this.conceptoSeleccionado,
      precio:this.precioConcepto,
      cantidad:this.cantidad,
      total:(this.cantidad * this.precioConcepto)
    }
    this.desglose.push(datos)
    this.desglose.forEach((element:any)=>{
      total = total + element.total
    })
    this.total = total
    this.concep = ''
    this.precioConcepto = 0
    this.cantidad = 0
  }

  obtenerPresidente() {
    this.president = localStorage.getItem('usuario');
    this.nombre = this.president.replace(/['"]+/g, '');
    this.id = localStorage.getItem('id')
    this.idPresidente = this.id.replace(/['"]+/g, '');
  }

  files: File[] = [];

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    /* this.files.forEach(element => {
      if(element.type != "image/jpeg"){
        this.onRemove(element);
        Swal.fire({
          icon: 'error',
          title: 'Lo sentimos',
          text: 'El archivo no es una imagen',
        })
      }
    }); */
    /* } */
  }

  onRemove(event: any) {
    this.files.splice(this.files.indexOf(event), 1);
  }

  atletass(event: any) {
    console.log(event);

    this.atleta = event;
  }

  equipoatlet(event: any) {
    console.log(event);

    this.equipo = event
  }

  entrenadores(event: any) {
    this.entrenador = event;
  }

  examinadores(event: any) {
    this.examinador = event;
  }

  juez(event: any) {
    this.jueces = event;
  }

  subirImagen(e: any) {
    const randomId = Math.random().toString(36).substring(2);
    const file = e;
    const filePath = `vouchers/${randomId}`;
    const ref = this._storage.ref(filePath);
    const task = this._storage.upload(filePath, file);
    task.snapshotChanges().pipe(
      finalize(() => {
        ref.getDownloadURL().subscribe(urlImage => {
          this.urlImage = urlImage;
          this.cargarImagen(urlImage);
        })
      })
    ).subscribe();
  }

  cargarImagen(url: any) {
    let data = {
      bouchers: url,
      presidente: this.idPresidente
    }
    this._registro.agregarImg(data).subscribe((resp: any) => {
      if (resp.ok) {
        console.log(resp);
      }
    })
  }

  /* pagos(event:any){
    this.files = event
  } */

  enviar() {
    Swal.fire({
      title: '¿Deseas guardar tus datos?',
      showDenyButton: true,
      confirmButtonText: `Guardar`,
      denyButtonText: `No Guardar`,
    }).then((result) => {
      if (result.isConfirmed) {
        const formData = new FormData();
        /* console.log(this.files[0]); */
          /* formData.append("bouchers", this.files[0]); */
          if (this.atleta.length != 0) {
            this.atleta.forEach(element => {
              let atlee = Object.assign(element, { presidente: this.nombre })
              /* let atlee = {
                valores : element,
                presidente: this.nombre
              } */
              this._registro.agregarAtle(atlee).subscribe((resp: any) => {
                if (resp.ok) {
                  console.log(resp);
                }
              })
            })
          }

          if (this.equipo.length != 0) {
            this.equipo.forEach((element: any) => {
              element.atletas.forEach((elements: any) => {
                let atlee = Object.assign(elements, { presidente: this.nombre })

                this._registro.agregarAtle(atlee).subscribe((resp: any) => {
                  if (resp.ok) {
                    console.log(resp);
                  }
                })
              });
            })
          }
          /* for (var i = 0; i <= this.files.length; i++) {
          } */

          this.examinador.forEach(element => {
            let exami = Object.assign(element, { presidente: this.nombre })
            this._registro.agregarExami(exami).subscribe((resp: any) => {
              if (resp.ok) {
                console.log(resp);
              }
            })
          })


          this.entrenador.forEach(element => {
            let entre = Object.assign(element, { presidente: this.nombre })
            this._registro.agregarEntre(entre).subscribe((resp: any) => {
              if (resp.ok) {
                console.log(resp);

              }
            })
          })

          this.jueces.forEach(element => {
            let juez = Object.assign(element, { presidente: this.nombre })
            this._registro.agregarJuecess(juez).subscribe((resp: any) => {
              if (resp.ok) {
                console.log(resp);

              }
            })
          })

          this.files.forEach((element) => {
            this.subirImagen(element);
          })

          Swal.fire({
            title: 'Datos Guardados Correctamente',
            icon: 'success',
            showDenyButton: false,
            confirmButtonText: `Guardar`,
          }).then((result) => {
            this.router.navigate(['/login'])
          })

          /* Swal.fire('Guardados Correctamente', '', 'success') */

        /* Swal.fire('El registro se encuentra cerrado', '', 'error') */
      } else if (result.isDenied) {
        Swal.fire('No se guardaron tus datos', '', 'error')
      }
    })
  }

}
