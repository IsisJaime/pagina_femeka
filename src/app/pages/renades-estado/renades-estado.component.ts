import { Component, OnInit } from '@angular/core';
import { EstadosService } from '../../services/estados/estados.service';
import { RenadesService } from '../../services/renades/renades.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-renades-estado',
  templateUrl: './renades-estado.component.html',
  styleUrls: ['./renades-estado.component.scss']
})
export class RenadesEstadoComponent implements OnInit {

  public estados:any[]=[];
  public renades:any[]=[];

  constructor(private _estados:EstadosService,
              private _renade:RenadesService) { }

  ngOnInit(): void {
    this.estados = this._estados.getEstados();
  }

  obtenerRenade(event:any){
    this._renade.getRenadeEstado(event.target.value).subscribe((resp:any)=>{
      if (resp.ok) {
        if(resp['respuesta'].length > 0){
          this.renades = resp['respuesta']
        }else{
          this.renades = [];
          Swal.fire('No cuenta con renades registrados', '', 'error')
        }
      }
    })
  }
}
