import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RenadesEstadoComponent } from './renades-estado.component';

describe('RenadesEstadoComponent', () => {
  let component: RenadesEstadoComponent;
  let fixture: ComponentFixture<RenadesEstadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RenadesEstadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RenadesEstadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
