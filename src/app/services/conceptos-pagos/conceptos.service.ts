import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConceptosService {

  public pagosConceptos:any[] = [
    {
      concepto:'AFILIACION A CUENTA',
      costo:0
    },{
      concepto:'RENADES',
      costo:250
    },{
      concepto:'RENADES A CUENTA',
      costo:0
    },{
      concepto:'CURSO ENTRENADORES',
      costo:400
    },{
      concepto:'CURSO ARBITRAJE',
      costo:400
    },{
      concepto:'COMPETENCIA C. NEGRA',
      costo:400
    },{
      concepto:'COMPETENCIA KYU',
      costo:375
    }
  ]
  constructor() { }

  getConceptos(){
    return this.pagosConceptos;
  }
}
