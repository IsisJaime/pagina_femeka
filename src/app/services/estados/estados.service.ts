import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EstadosService {

  public estados:any[] = [
    {
      nombre:'Aguascalientes',
      valor:'Aguascalientes'
    },
    {
      nombre:'Baja California',
      valor:'Baja California'
    },
    {
      nombre:'Baja California Sur',
      valor:'Baja California Sur'
    },
    {
      nombre:'Campeche',
      valor:'Campeche'
    },
    {
      nombre:'Chiapas',
      valor:'Chiapas'
    },
    {
      nombre:'Chihuahua',
      valor:'Chihuahua'
    },
    {
      nombre:'Ciudad de México',
      valor:'Ciudad de Mexico'
    },
    {
      nombre:'Coahuila',
      valor:'Coahuila'
    },
    {
      nombre:'Colima',
      valor:'Colima'
    },
    {
      nombre:'Durango',
      valor:'Durango'
    },
    {
      nombre:'Estado de México',
      valor:'Estado de Mexico'
    },
    {
      nombre:'Guanajuato',
      valor:'Guanajuato'
    },
    {
      nombre:'Guerrero',
      valor:'Guerrero'
    },
    {
      nombre:'Hidalgo',
      valor:'Hidalgo'
    },
    {
      nombre:'Jalisco',
      valor:'Jalisco'
    },
    {
      nombre:'Michoacán',
      valor:'Michoacan'
    },
    {
      nombre:'Morelos',
      valor:'Morelos'
    },
    {
      nombre:'Nayarit',
      valor:'Nayarit'
    },
    {
      nombre:'Nuevo León',
      valor:'Nuevo Leon'
    },
    {
      nombre:'Oaxaca',
      valor:'Oaxaca'
    },
    {
      nombre:'Puebla',
      valor:'Puebla'
    },
    {
      nombre:'Querétaro',
      valor:'Queretaro'
    },
    {
      nombre:'Quintana Roo',
      valor:'Quintana Roo'
    },
    {
      nombre:'San Luis Potosí',
      valor:'San Luis Potosi'
    },
    {
      nombre:'Sinaloa',
      valor:'Sinaloa'
    },
    {
      nombre:'Sonora',
      valor:'Sonora'
    },
    {
      nombre:'Tabasco',
      valor:'Tabasco'
    },
    {
      nombre:'Tamaulipas',
      valor:'Tamaulipas'
    },
    {
      nombre:'Tlaxcala',
      valor:'Tlaxcala'
    },
    {
      nombre:'Veracruz',
      valor:'Veracruz'
    },
    {
      nombre:'Yucatán',
      valor:'Yucatan'
    },
    {
      nombre:'Zacatecas',
      valor:'Zacatecas'
    }
  ];

  constructor() { }

  getEstados(){
    return this.estados;
  }
}
