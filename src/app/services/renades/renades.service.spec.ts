import { TestBed } from '@angular/core/testing';

import { RenadesService } from './renades.service';

describe('RenadesService', () => {
  let service: RenadesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RenadesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
