import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { URL } from '../../config/config';

@Injectable({
  providedIn: 'root'
})
export class RenadesService {

  public url;

  constructor(private _http:HttpClient){
    this.url = URL;
  }

  agregarRenade(body:any){
    const url = `${this.url}/registro/renades`;
    return this._http.post( url, body );
  }

  getRenadeEstado(estado:any){
    const url = `${this.url}/renade/estado/${estado}`;
    return this._http.get( url );
  }

  getRenadeId(id:any){
    const url = `${this.url}/obtener/renade/${id}`;
    return this._http.get( url );
  }

  updateRenade(id:any, body:any){
    const url = `${this.url}/actualiza/renade/${id}`;
    return this._http.put( url, body );
  }

  getRenadeCurp(curp:any){
    const url = `${this.url}/validar/renade/${curp}`;
    return this._http.get( url );
  }
}
