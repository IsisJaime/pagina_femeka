import { Injectable } from '@angular/core';
import { URL } from '../config/config';
import  {  HttpClient  } from '@angular/common/http';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  public usuario: any;
  public url = URL;

  constructor( public http: HttpClient) { }

  login( personal: any ) {
    const url = `${this.url}/login`;
    return this.http.post( url, personal ).pipe(map((resp: any)=>{
      localStorage.setItem('usuario', JSON.stringify(resp.entrar.nombre) )
      localStorage.setItem('role', JSON.stringify(resp.entrar.role) )
      localStorage.setItem('id', JSON.stringify(resp.entrar._id))
      localStorage.setItem('estado',JSON.stringify(resp.entrar.estado))
      this.usuario = JSON.stringify( localStorage.getItem('usuario') );
      return resp;
    }));
  }

  agregarPresi(body:any){
    const url = `${this.url}/login/usuario`;
    return this.http.get( url, body );
  }
}
