import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  menu: any[] = [
    {
      titulo: 'Dashboard',
      icono: 'mdi mdi-gauge',
      submenu: [
        { titulo: 'KATA', url: 'registro/competidores' },
        { titulo: 'KUMITE', url: 'registro/kumite' },
        { titulo: 'DOCUMENTOS', url: 'documentos' },
        { titulo: 'GRAFICAS KATA', url: 'graficas/kumite' },
        { titulo: 'LOGOUT', url: '/login' },

      ]
    },
  ];

  constructor() { }
}