import { Injectable } from '@angular/core';
import { URL } from '../../config/config';
import  {  HttpClient  } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class Presidentes {
  public url = URL;

  constructor(private _http:HttpClient) { }

  obtenerPresidentes(){
    const url = `${this.url}/obtener/presidentes`;
    return this._http.get(url);
  }

  obtenerPresidenteEstado(id:any){
    const url = `${this.url}/encontrar/presidente/${id}`;
    return this._http.get(url);
  }

  actualizarRenades(id:any, body:any){
    let url = `${this.url}/actualiza/presidente/${id}`;
    return this._http.put(url,body);
  }

  findRenade(curp:any){
    const url = `${this.url}/validar/renade/${curp}`;
    return this._http.get(url);
  }

}
