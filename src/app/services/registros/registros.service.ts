import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { URL } from '../../config/config';


@Injectable({
  providedIn: 'root'
})
export class RegistrosService {

  public url;

  constructor(private _http:HttpClient) {
    this.url = URL;
  }

  agregarAtle(body:any){
    console.log(body);

    const url = `${this.url}/atleta/nuevo`;
    return this._http.post( url, body );
  }

  agregarEntre(body:any){
    console.log(body);

    const url = `${this.url}/agregar/nuevo/entrenador`;
    return this._http.post( url, body );
  }

  agregarExami(body:any){
    console.log(body);

    const url = `${this.url}/agregar/nuevo/examinador`;
    return this._http.post( url, body );
  }

  agregarJuecess(body:any){
    console.log(body);

    const url = `${this.url}/agregar/nuevo/juez`;
    return this._http.post( url, body );
  }

  /* agregarImg(img:any,pre:any){
    const nom = 'img';
    let url = `${this.url}/subir/archivo/`+`${nom}`+`/`+`${img}`+`/`+`${pre}` ;
    return this._http.put(url,img)
  } */

  agregarImg(body:any){
    let url = `${this.url}/subir/img/vocuher`;
    return this._http.post(url,body)
  }


  obtenerAtletas(){
    const url = `${this.url}/obetner/atletas`;
    return this._http.get( url );
  }

  obtenerJueces(){
    const url = `${this.url}/obtener/juces`;
    return this._http.get( url );
  }

  obtenerEntrenadores(){
    const url = `${this.url}/obtener/entrenador`;
    return this._http.get( url );
  }

  obtenerExaminadores(){
    const url = `${this.url}/obtener/examinadores`;
    return this._http.get( url );
  }

  obtenerCategoriasM(){
    const url = `${this.url}/obtener/negra/kata/Hombres`;
    return this._http.get( url );
  }

  obtenerCategoriasF(){
    const url = `${this.url}/obtener/negra/mujeres`;
    return this._http.get( url );
  }

  obtenerCategoriasKyuM(){
    const url = `${this.url}/obtener/grados/kyu/Hombres`;
    return this._http.get( url );
  }

  obtenerCategoriasKyuF(){
    const url = `${this.url}/obtener/grados/kyu/mujeres`;
    return this._http.get( url );
  }

  obtenerCategoriaKumiteHombres(){
    const url = `${this.url}/obtener/categorias/kumite/negra/hombre`;
    return this._http.get( url );
  }

  obtenerCategoriaKumiteMujeres(){
    const url = `${this.url}/obtener/categorias/kumite/negra/Mujer`;
    return this._http.get( url );
  }

  obtenerCategoriaParakarate(){
    const url = `${this.url}/obtener/categorias/kata/parakarate`;
    return this._http.get( url );
  }

  obtenerDivisiones(){
    const url = `${this.url}/obtener/divisiones`;
    return this._http.get( url );
  }

  obtenerImg(foto:any ){
    const url = `${this.url}/ver/img/${foto}`;
    return this._http.get(  url );
  }

  obtenerIdPresidente(estado:any){
    const url = `${this.url}/obtener/presidente/${estado}`;
    return this._http.get(  url );
  }

  obtenerUrlImg(id:any){
    const url = `${this.url}/obtener/imagen/presidente/${id}`;
    return this._http.get(  url );
  }

  editarAtleta(id:any,body:any){
    let url = `${this.url}/actualiza/atleta/${id}`;
    return this._http.put(url,body);
  }

  eliminarAtleta(id:any){
    const url = `${this.url}/eliminar/atleta/${id}`;
    return this._http.delete(  url );
  }

  editarEntrena(id:any,body:any){
    let url = `${this.url}/actualiza/entrenador/${id}`;
    return this._http.put(url,body);
  }

  eliminarEntrena(id:any){
    const url = `${this.url}/eliminar/entrenador/${id}`;
    return this._http.delete(  url );
  }

  editarExamina(id:any,body:any){
    let url = `${this.url}/actualiza/examinador/${id}`;
    return this._http.put(url,body);
  }

  eliminarExamina(id:any){
    const url = `${this.url}/eliminar/examinador/${id}`;
    return this._http.delete(  url );
  }

  editarJueces(id:any,body:any){
    let url = `${this.url}/actualiza/juez/${id}`;
    return this._http.put(url,body);
  }

  eliminarJueces(id:any){
    const url = `${this.url}/eliminar/juez/${id}`;
    return this._http.delete(  url );
  }

  obtenerCategoria(body:any){
    let url = `${this.url}/categorias/individual/atleta`;
    return this._http.post(url,body)
  }
}
