import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';
import { Personal } from '../../interfaces/Personal';

import Swal from 'sweetalert2';
import {  NgForm  } from '@angular/forms';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

//cambio

export class LoginComponent implements OnInit {
  showMyContainer: boolean = false;
  constructor(private router: Router, public _loginService: LoginService,) {
   // this.loadScripts();
  }

  public role = '';

  ngOnInit(): void {
    this.role = JSON.stringify( localStorage.getItem('role') );

   
      if (!localStorage.getItem('foo')) { 
        localStorage.setItem('foo', 'no reload') 
        location.reload() 
      } else {
        localStorage.removeItem('foo') 
      }
    

  }

  loadScripts() {
    const dynamicScripts = [
      './assets/plugins/jquery/jquery.min.js',
      './assets/plugins/bootstrap/js/bootstrap.min.js',
      './assets/js/perfect-scrollbar.jquery.min.js',
      './assets/js/waves.js',
      './assets/js/sidebarmenu.js',
      './assets/plugins/sticky-kit-master/dist/sticky-kit.min.js',
      './assets/plugins/sparkline/jquery.sparkline.min.js',
      './assets/js/custom.js',
      './assets/plugins/styleswitcher/jQuery.style.switcher.js',
      './assets/js_pagina/header.js',
      'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js',
      './assets/js_pagina/accordion.js',
      './assets/js_pagina/fondo_admin.js',
      'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js',
      './assets/js/menu.js',
      'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js',
      './assets/js/card-slider.js',
      './assets/js/loading.js',
      './assets/js/preloader.js',
      'https://maps.googleapis.com/maps/api/js?key=undefined',
      'https://www.gstatic.com/charts/loader.js',
      'https://www.google.com/jsapi',
      './assets/js/geochart.js',
      './assets/js/calendar.js',
      './assets/js/slider-home.js',
      'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v12.0',
      'https://snapwidget.com/js/snapwidget.js',
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

  login() {
     /* this.router.navigate(['/dashboard']) */
  }


ingresar( forma: NgForm ): any {

   /* this.router.navigate(['/dashboard']) */

    if( forma.invalid ){
      Swal.fire('Llena todos los campos','','error');
      return false;
    }

   const nombrePersonal = forma.value.nombre.trim();
   const passwordPersonal = forma.value.password.trim();

    if( !nombrePersonal ){
      Swal.fire('Nombre no valido', 'Ingresalo de nuevo', 'error');
      forma.reset();
      return
    }
    else if(!passwordPersonal){
      Swal.fire('Contraseña no valida', 'Ingresalo de nuevo', 'error');
      forma.reset();
      return
    }

    let personal = new Personal( nombrePersonal, passwordPersonal );

    this._loginService.login( personal )
    .subscribe( (correcto: any)=> {

      if(!correcto){
        Swal.fire('El nombre o la contraseña son incorrectos', 'Ingresalos de nuevo', 'error');
      }

      if( correcto ){

        /* console.log(correcto) */


      }
    },
      error => {
          if( error['ok']  == false){
            forma.reset();
            Swal.fire('El nombre o la contraseña son incorrectos', 'Ingresalos de nuevo', 'error');
          }
      }
     )


}



}







