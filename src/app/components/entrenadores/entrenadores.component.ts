import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { RegistrosService } from '../../services/registros/registros.service';
import { Presidentes } from 'src/app/services/presidentes/presidentes.service';
import Swal from 'sweetalert2';
import { calculateAge, fechaCurp2, curpValida } from '../../functions/curpFecha';
@Component({
  selector: 'app-entrenadores',
  templateUrl: './entrenadores.component.html',
  styleUrls: ['./entrenadores.component.scss']
})
export class EntrenadoresComponent implements OnInit {

  @Output() entrenadores = new EventEmitter<string>();
  public entrenador:any = [];
  public estado = '';
  public entrenadorEstado:any [] = [];
  public entrenadoresRegistrados:any[]=[];
  public validarRenade = false;
  public entrena = {
    nombre:'',
    apellidoPaterno:'',
    apellidoMaterno:'',
    sexo:'',
    estado:'',
    gradoCN:'',
    estilo:'',
    fechaNacimiento:'',
    curp:''
  }
  public botonEnviar = true;
  public botonEdit = false;
  public idEntrenador = '';

  constructor(private _registro:RegistrosService,
              private _renade:Presidentes) {}

  ngOnInit(): void {
    this.allEntrenadores();
  }

  buscarEntrenador(){
    this._renade.findRenade(this.entrena.curp.toUpperCase()).subscribe((resp:any)=>{
      if (resp.ok) {
        if (resp['respuesta'].length != 0) {
          this.validarRenade = true;
          this.setEntrenadorRenade(resp['respuesta'][0])
        }else{
          Swal.fire('El atleta que trata de ingresar no cuenta con renade', '', 'error')
          this.entrena.curp='';
          this.validarRenade = false
        }
      }
    })
  }

  setEntrenadorRenade(data:any){
    this.entrena.nombre = data.nombre
    this.entrena.apellidoPaterno = data.apellidoPaterno
    this.entrena.apellidoMaterno = data.apellidoMaterno
    this.entrena.sexo = data.genero
    this.entrena.estado = data.estado
    this.entrena.fechaNacimiento = data.fechaNacimiento
  }


  allEntrenadores() {
    this._registro.obtenerEntrenadores().subscribe((data: any) => {
      this.entrenadorEstado = data.respuesta;
      this.obtenerEntrenadoresRegistrados();
    })
  }

  obtenerEntrenadoresRegistrados(){
    this.estado = JSON.parse(localStorage.getItem('usuario')!)
    this.entrenadorEstado.forEach((element:any)=>{
      if(element.presidente == this.estado){
        this.entrenadoresRegistrados.push(element)
      }
    })
  }

  formularioEntrenadores(form: NgForm){
    this.entrenador.push(form.value)
    this.entrenadores.emit(this.entrenador)
    var rest = 'entrenadoresForm';
    var Form = <HTMLFormElement>document.getElementById(rest);
    Form.reset();
    this.validarRenade = false
  }

  editEntrenador(item:any){
    console.log(item);
    this.validarRenade = true;
    this.entrena.nombre = item.nombre
    this.entrena.apellidoPaterno = item.apellidoPaterno
    this.entrena.apellidoMaterno = item.apellidoMaterno
    this.entrena.sexo = item.sexo
    this.entrena.estado = item.estado
    this.entrena.fechaNacimiento = item.fechaNacimiento
    this.entrena.curp = item.curp
    this.entrena.estilo = item.estilo
    this.entrena.gradoCN = item.gradoCN
    this.idEntrenador = item._id;
    this.botonEnviar = false;
    this.botonEdit = true;
  }
  deleteEntrenador(id:any){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'ESTAS SEGURO DE ELIMINAR EL ATLETA?',
      text: "LOS CAMBIOS REALIZADOS YA NO SE PODRAN RECUPERAR",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'ELIMINAR',
      cancelButtonText: 'CANCELAR',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.entrenadoresRegistrados=[];
        this._registro.eliminarEntrena(id).subscribe((resp:any)=>{
            swalWithBootstrapButtons.fire(
              'ELIMINADO!',
              'EL ENTRENADOR FUE ELIMINADO CORRECTAMENTE.',
              'success'
            )
            this.allEntrenadores();
        })
      }else if (result.dismiss === Swal.DismissReason.cancel){
        swalWithBootstrapButtons.fire(
          'CANCELADO',
          'LOS DATOS NO SE MODIFICARON',
          'error'
        )
      }
    })
  }
  edit(form:NgForm){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'ESTAS SEGURO DE ACTUALIZAR EL ATLETA?',
      text: "LOS CAMBIOS REALIZADOS YA NO SE PODRAN RECUPERAR",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'EDITAR',
      cancelButtonText: 'CANCELAR',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.entrenadoresRegistrados=[];
        this._registro.editarEntrena(this.idEntrenador,form.value).subscribe((resp:any)=>{
          swalWithBootstrapButtons.fire(
            'ACTUALIZADO!',
            'EL ENTRENADOR FUE ACTUALIZADO CORRECTAMENTE.',
            'success'
          )
          this.allEntrenadores();
          var dirtyFormID = 'formulario';
          var resetForm = <HTMLFormElement>document.getElementById(dirtyFormID);
          resetForm.reset();
          this.validarRenade = false;
          this.botonEnviar = true;
          this.botonEdit = false;
          this.entrena.curp = '';
        })
      }else if (result.dismiss === Swal.DismissReason.cancel){
        swalWithBootstrapButtons.fire(
          'CANCELADO',
          'LOS DATOS NO SE MODIFICARON',
          'error'
        )
      }
    })
  }

}
