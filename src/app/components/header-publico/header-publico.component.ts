import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';
import { Personal } from '../../interfaces/Personal';
import { NgForm } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2';


@Component({
  selector: 'app-header-publico',
  templateUrl: './header-publico.component.html',
  styleUrls: ['./header-publico.component.scss']
})

export class HeaderPublicoComponent implements OnInit {

  showMyContainer: boolean = false;
  public role = '';

  constructor(private router: Router,
              public _loginService: LoginService,
              private spinner: NgxSpinnerService) {}

  ngOnInit(): void {
    this.role = JSON.stringify(localStorage.getItem('role'));
  }

  ingresar(forma: NgForm): any {
    this.spinner.show();
    if( forma.invalid ){
      Swal.fire('Llena todos los campos','','error');
      return false;
    }
    const nombrePersonal = forma.value.nombre.trim();
    const passwordPersonal = forma.value.password.trim();

    if (!nombrePersonal) {
      Swal.fire('Nombre no valido', 'Ingresalo de nuevo', 'error');
      forma.reset();
      return
    }
    else if (!passwordPersonal) {
      Swal.fire('Contraseña no valida', 'Ingresalo de nuevo', 'error');
      forma.reset();
      return
    }

    let personal = new Personal( nombrePersonal, passwordPersonal );

    this._loginService.login(personal).subscribe((correcto: any) => {
      if( correcto.ok ){
        if (correcto.entrar.role == "admin"){
          /* Swal.fire('No cuentas con los permisos', '', 'error') */
          this.router.navigate(['/dashboard/admin'])
          this.spinner.hide();
        }else if(correcto.entrar.role == "Super usuario"){
          this.router.navigate(['/dashboard/super/admin']);
          this.spinner.hide();
        }else{
          this.router.navigate(['/dashboard'])
          this.spinner.hide();
          /* Swal.fire('Se finalizaron los registros', '', 'error') */
        }
      }
    },
    error => {
      if( error['ok']  == false){
        forma.reset();
        Swal.fire('El nombre o la contraseña son incorrectos', 'Ingresalos de nuevo', 'error');
      }
    })
  }
}







