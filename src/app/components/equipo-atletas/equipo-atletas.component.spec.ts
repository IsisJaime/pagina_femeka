import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipoAtletasComponent } from './equipo-atletas.component';

describe('EquipoAtletasComponent', () => {
  let component: EquipoAtletasComponent;
  let fixture: ComponentFixture<EquipoAtletasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EquipoAtletasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipoAtletasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
