import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { atlet } from 'src/app/interfaces/atleta';
import { Presidentes } from 'src/app/services/presidentes/presidentes.service';
import Swal from 'sweetalert2';
import { NgForm } from '@angular/forms';
import { RegistrosService } from 'src/app/services/registros/registros.service';
import { atletasFilter, modalidad } from '../../functions/atletasEdad';


@Component({
  selector: 'app-equipo-atletas',
  templateUrl: './equipo-atletas.component.html',
  styleUrls: ['./equipo-atletas.component.scss']
})
export class EquipoAtletasComponent implements OnInit {

  @Output() equipoatlet = new EventEmitter();
  public deshabilitar = false;
  public atletaR: atlet = {} as atlet;
  public activarDatos = false;
  public atletEquipo:any=[];
  public equipoAtleta:any=[];
  @ViewChild('name') inputName:any;
  @ViewChild('apellidoPaterno') inputName1:any;
  @ViewChild('apellidoMaterno') inputName2:any;
  @ViewChild('edad') inputName4:any;

  @ViewChild('modalidad') moda:any;
  @ViewChild('grado') grado:any;
  @ViewChild('divicion') divicion:any;
  @ViewChild('categori') categori:any;
  @ViewChild('clase') clase:any;
  @ViewChild('nombreEqui') equipo:any;
  public atletasEquipoRegistrados:any[]=[];
  public activarDivisiones = false;
  public divisiones:any [] = [];
  public categorias:any [] = [];

  constructor(private _renade:Presidentes,
              private _registro:RegistrosService) { }

  ngOnInit(): void {
    this.allAtletas();
  }

  allAtletas(){
    this._registro.obtenerAtletas().subscribe((resp: any) => {
      this.obtenerAtletasRegistrados(resp.respuesta);
    })
  }

  obtenerAtletasRegistrados(data:any){
    let estado = JSON.parse(localStorage.getItem('estado')!)
    data.forEach((element:any)=>{
      if(element.presidente.toUpperCase() == estado.toUpperCase()){
        if(element.integrantes.toUpperCase() == "POR EQUIPOS"){
          this.atletasEquipoRegistrados.push(element)
        }
      }
    })
  }

  buscarAtleta(){
    this.deshabilitar = true
    this._renade.findRenade(this.atletaR.curp.toUpperCase()).subscribe((resp:any)=>{
      if (resp.ok) {
        if (resp['respuesta'].length != 0) {
          this.activarDatos = true;
          this.setAtletaRenade(resp['respuesta'][0])
        }else{
          Swal.fire('El atleta que trata de ingresar no cuenta con renade', '', 'error')
          this.atletaR.curp='';
          this.activarDatos = false
          this.deshabilitar = false;
        }
      }
    })
  }

  setAtletaRenade(data:any){
    this.atletaR.nombre = data.nombre
    this.atletaR.apellidoPaterno = data.apellidoPaterno
    this.atletaR.apellidoMaterno = data.apellidoMaterno
    this.atletaR.edad = data.edad
    this.atletaR.sexo = data.genero
    this.atletaR.estado = data.estado
  }

  obtenerDivisiones(event:any){
    if (event.target.value == 'GRADOS KYU') {
      this.activarDivisiones = true
      this._registro.obtenerDivisiones().subscribe((resp:any)=>{
        if (resp.ok) {
          this.divisiones = resp['Divi']
        }
      });
    }else{
      this.activarDivisiones = false
      this.divisiones = [];
    }
  }

  formularioAtletas(form: NgForm){
    let valor = Object.assign(form.value, modalidad(form.value.categoria));
    if (form.form.status == 'INVALID') {
      Swal.fire('Es necesario llenar todos los campos del atleta', '', 'error')
    }else if (form.form.status == 'VALID'){
      if(form.form.value.modalidad == 'KUMITE'){
        if(form.form.value.sexo == 'MASCULINO'){
          if(this.atletEquipo.length == 4){
            this.atletEquipo.push(valor)
            this.finalizarEquipo(form);
            Swal.fire('Se completo con exito el equipo', '', 'success')
          }else{
            this.atletEquipo.push(valor)
            Swal.fire('Agrega el siguiente atleta', '', 'success')
          }
        }else{
          if(this.atletEquipo.length == 2){
            this.atletEquipo.push(valor)
            this.finalizarEquipo(form);
            Swal.fire('Se completo con exito el equipo', '', 'success')
          }else{
            this.atletEquipo.push(valor)
            Swal.fire('Agrega el siguiente atleta', '', 'success')
          }
        }
      }else{
        if(this.atletEquipo.length == 2){
          this.atletEquipo.push(valor)
          this.finalizarEquipo(form);
          Swal.fire('Se completo con exito el equipo', '', 'success')
        }else{
          this.atletEquipo.push(valor)
          Swal.fire('Agrega el siguiente atleta', '', 'success')
        }
      }
      /* var dirtyFormID = 'formulario';
      var resetForm = <HTMLFormElement>document.getElementById(dirtyFormID);*/
      this.inputName.nativeElement.value = '';
      this.inputName1.nativeElement.value = '';
      this.inputName2.nativeElement.value = '';
      this.atletaR.curp='';
      this.inputName4.nativeElement.value = '';
      this.activarDatos = false
      this.deshabilitar = false
    }
  }

  finalizarEquipo(form: NgForm){
    if (form.form.status == 'INVALID') {
      Swal.fire('Es necesario completar el formulario', '', 'error')
    }else{
      let equipo = {
        nombreEquipo:'',
        categoria:'',
        atletas:[]
      }
      equipo.nombreEquipo = form.form.value.equipo
      equipo.atletas = this.atletEquipo
      equipo.categoria = form.form.value.categoria
      this.equipoAtleta.push(equipo);
      this.equipoatlet.emit(this.equipoAtleta);
      this.atletEquipo = [];

      /* this.moda.nativeElement.value = ''; */
      this.grado.nativeElement.value = '';
      /* this.divicion.nativeElement.value = ''; */
      this.categori.nativeElement.value = '';
      /* this.clase.nativeElement.value = ''; */
      this.inputName.nativeElement.value = '';
      this.inputName1.nativeElement.value = '';
      this.inputName2.nativeElement.value = '';
      this.atletaR.curp='';
      this.inputName4.nativeElement.value = '';
      this.activarDatos = false
      this.deshabilitar = false
      this.equipo.nativeElement.value = '';
    }
  }

  /* editAtleta(atleta:any){
    this.atletaR.nombre = atleta.nombre;
    this.atletaR.apellidoPaterno = atleta.apellidoPaterno;
    this.atletaR.apellidoMaterno = atleta.apellidoMaterno;
    this.atletaR.curp = atleta.curp;
    this.atletaR.edad = atleta.edad;
    this.atletaR.sexo = atleta.sexo;
    this.atletaR.estado = atleta.estado;
    this.moda.nativeElement.value = atleta.modalidad;
    this.atletaR.colorCinta=atleta.colorCinta;
    this.grados2(this.atletaR);
    this.atletaR.division=atleta.division;
    this.atletaR.categoria=atleta.categoria;
    this.equi2(this.atletaR.categoria);
    this.atletaR.integrantes=atleta.integrantes;
    this.equipoo2(this.atletaR.integrantes);
    this.atletaR.equipo=atleta.equipo;
    this.idAtlet = atleta._id;
  } */

  /* edit(form:NgForm){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'ESTAS SEGURO DE ACTUALIZAR EL ATLETA?',
      text: "LOS CAMBIOS REALIZADOS YA NO SE PODRAN RECUPERAR",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'EDITAR',
      cancelButtonText: 'CANCELAR',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.atletasEquipoRegistrados=[];
        this._registro.editarAtleta(this.idAtlet,form.value).subscribe((resp:any)=>{
          swalWithBootstrapButtons.fire(
            'ACTUALIZADO!',
            'EL ALTETA FUE ACTUALIZADO CORRECTAMENTE.',
            'success'
          )
          this.allAtletas();
          var dirtyFormID = 'formulario';
          var resetForm = <HTMLFormElement>document.getElementById(dirtyFormID);
          resetForm.reset();
        })
      }else if (result.dismiss === Swal.DismissReason.cancel){
        swalWithBootstrapButtons.fire(
          'CANCELADO',
          'LOS DATOS NO SE MODIFICARON',
          'error'
        )
      }
    })
  }

  deleteAtleta(id:any){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'ESTAS SEGURO DE ELIMINAR EL ATLETA?',
      text: "LOS CAMBIOS REALIZADOS YA NO SE PODRAN RECUPERAR",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'ELIMINAR',
      cancelButtonText: 'CANCELAR',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.atletasEquipoRegistrados=[];
        this._registro.eliminarAtleta(id).subscribe((resp:any)=>{
            swalWithBootstrapButtons.fire(
              'ELIMINADO!',
              'EL ALTETA FUE ELIMINADO CORRECTAMENTE.',
              'success'
            )
            this.allAtletas();
        })
      }else if (result.dismiss === Swal.DismissReason.cancel){
        swalWithBootstrapButtons.fire(
          'CANCELADO',
          'LOS DATOS NO SE MODIFICARON',
          'error'
        )
      }
    })
  } */

}
