import { Component, OnInit, Output, EventEmitter, ViewChild} from '@angular/core';
import { NgForm } from '@angular/forms';
import { RegistrosService } from '../../services/registros/registros.service';
import Swal from 'sweetalert2';
import { Presidentes } from '../../services/presidentes/presidentes.service';
import { atlet } from '../../interfaces/atleta';
import { atletasFilter } from '../../functions/atletasEdad';

@Component({
  selector: 'app-atleta',
  templateUrl: './atleta.component.html',
  styleUrls: ['./atleta.component.scss']
})

export class AtletaComponent implements OnInit {

  @Output() atletass = new EventEmitter();
  public activarDatos = false
  public atletaR: atlet = {} as atlet;
  public activarDivisiones = false;
  public divisiones:any [] = [];
  public categorias:any [] = [];
  public atlet:any[] = []
  public deshabilitar = false;
  public atletasRegistrados:any[]=[];
  public btnEdit = true;
  public modalidad = '';
  public colorCinta = '';
  public division = '';
  public categoria = '';
  public enviarFr = true;
  public idAtlet = '';

  constructor(private _registro:RegistrosService,
              private _renade:Presidentes) { }

  ngOnInit(): void {
    this.allAtletas();
  }

  allAtletas(){
    this._registro.obtenerAtletas().subscribe((resp: any) => {
      this.obtenerAtletasRegistrados(resp.respuesta);
    })
  }

  obtenerAtletasRegistrados(data:any){
    let estado = JSON.parse(localStorage.getItem('usuario')!)
    data.forEach((element:any)=>{
      if(element.presidente.toUpperCase() == estado.toUpperCase()){
        if(element.integrantes.toUpperCase() == "INDIVIDUAL"){
          this.atletasRegistrados.push(element)
        }
      }
    })
  }

  buscarAtleta(){
    this.deshabilitar = true
    this._renade.findRenade(this.atletaR.curp.toUpperCase()).subscribe((resp:any)=>{
      if (resp.ok) {
        if (resp['respuesta'].length != 0) {
          this.activarDatos = true;
          this.setAtletaRenade(resp['respuesta'][0])
        }else{
          Swal.fire('El atleta que trata de ingresar no cuenta con renade', '', 'error')
          this.atletaR.curp='';
          this.activarDatos = false
        }
      }
    })
  }

  setAtletaRenade(data:any){
    this.atletaR.nombre = data.nombre
    this.atletaR.apellidoPaterno = data.apellidoPaterno
    this.atletaR.apellidoMaterno = data.apellidoMaterno
    this.atletaR.edad = data.edad
    this.atletaR.sexo = data.genero
    this.atletaR.estado = data.estado
  }

  obtenerCategoria(){
    this._registro.obtenerCategoria({genero:this.atletaR.sexo}).subscribe((resp:any)=>{
      if (resp.ok) {
        this.setCategorias(resp['categorias'])
      }
    })
  }

  setCategorias(data:any){
    this.categorias = atletasFilter(data,this.atletaR.edad);
  }

  obtenerDivisiones(event:any){
    this.obtenerCategoria();
    if (event.target.value == 'GRADOS KYU') {
      this.activarDivisiones = true
      this._registro.obtenerDivisiones().subscribe((resp:any)=>{
        if (resp.ok) {
          this.divisiones = resp['Divi']
        }
      });
    }else{
      this.activarDivisiones = false
      this.divisiones = [];
    }
  }

  formularioAtletas(form: NgForm){
    if (form.form.status == 'INVALID') {
      Swal.fire('Es necesario llenar todos los campos del atleta', '', 'error')
    }else if (form.form.status == 'VALID'){
      this.atlet.push(Object.assign(form.value,{curp:this.atletaR.curp},{integrantes:'INDIVIDUAL'}))
      this.atletass.emit(this.atlet);
      this.activarDivisiones = false
      this.divisiones = [];
      this.activarDatos = false
      var dirtyFormID = 'formulario';
      var resetForm = <HTMLFormElement>document.getElementById(dirtyFormID);
      resetForm.reset();
    }
  }

  editAtleta(item:any){
    this.activarDatos = true;
    this.atletaR.curp = item.curp
    this.atletaR.nombre = item.nombre
    this.atletaR.apellidoPaterno = item.apellidoPaterno
    this.atletaR.apellidoMaterno =  item.apellidoMaterno
    this.atletaR.edad = item.edad
    this.atletaR.sexo = item.sexo
    this.atletaR.estado = item.estado
    this.modalidad = item.modalidad
    this.colorCinta = item.colorCinta
    this.obtenerCategoria();
    if(item.division != ''){
      this.activarDivisiones = true
      this._registro.obtenerDivisiones().subscribe((resp:any)=>{
        if (resp.ok) {
          this.divisiones = resp['Divi']
        }
      });
      this.division = item.division;
    }
    this.categoria = item.categoria;
    this.idAtlet = item._id;
    this.enviarFr = false
  }
  deleteAtleta(id:any){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'ESTAS SEGURO DE ELIMINAR EL ATLETA?',
      text: "LOS CAMBIOS REALIZADOS YA NO SE PODRAN RECUPERAR",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'ELIMINAR',
      cancelButtonText: 'CANCELAR',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.atletasRegistrados=[];
        this._registro.eliminarAtleta(id).subscribe((resp:any)=>{
            swalWithBootstrapButtons.fire(
              'ELIMINADO!',
              'EL ALTETA FUE ELIMINADO CORRECTAMENTE.',
              'success'
            )
            this.allAtletas();
        })
      }else if (result.dismiss === Swal.DismissReason.cancel){
        swalWithBootstrapButtons.fire(
          'CANCELADO',
          'LOS DATOS NO SE MODIFICARON',
          'error'
        )
      }
    })
  }
  edit(form:NgForm){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'ESTAS SEGURO DE ACTUALIZAR EL ATLETA?',
      text: "LOS CAMBIOS REALIZADOS YA NO SE PODRAN RECUPERAR",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'EDITAR',
      cancelButtonText: 'CANCELAR',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.atletasRegistrados=[];
        this._registro.editarAtleta(this.idAtlet,form.value).subscribe((resp:any)=>{
          swalWithBootstrapButtons.fire(
            'ACTUALIZADO!',
            'EL ALTETA FUE ACTUALIZADO CORRECTAMENTE.',
            'success'
          )
          this.allAtletas();
          var dirtyFormID = 'formulario';
          var resetForm = <HTMLFormElement>document.getElementById(dirtyFormID);
          resetForm.reset();
          this.btnEdit = false;
          this.activarDatos = false;
          this.activarDivisiones = false;
          this.enviarFr = true;
        })
      }else if (result.dismiss === Swal.DismissReason.cancel){
        swalWithBootstrapButtons.fire(
          'CANCELADO',
          'LOS DATOS NO SE MODIFICARON',
          'error'
        )
      }
    })
  }
}
