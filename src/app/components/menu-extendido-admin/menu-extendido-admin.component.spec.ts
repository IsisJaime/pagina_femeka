import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuExtendidoAdminComponent } from './menu-extendido-admin.component';

describe('MenuExtendidoAdminComponent', () => {
  let component: MenuExtendidoAdminComponent;
  let fixture: ComponentFixture<MenuExtendidoAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuExtendidoAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuExtendidoAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
