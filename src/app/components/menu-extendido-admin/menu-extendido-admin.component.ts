import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-menu-extendido-admin',
  templateUrl: './menu-extendido-admin.component.html',
  styleUrls: ['./menu-extendido-admin.component.scss']
})
export class MenuExtendidoAdminComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  cerrar(){
    localStorage.clear();
    this.router.navigate(['/login'])

    var landingUrl = "/login"; 
    window.location.href = landingUrl;
  }
}
