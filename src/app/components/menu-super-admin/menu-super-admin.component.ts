import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu-super-admin',
  templateUrl: './menu-super-admin.component.html',
  styleUrls: ['./menu-super-admin.component.scss']
})
export class MenuSuperAdminComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  cerrar(){
    localStorage.clear();
    this.router.navigate(['/login'])

    var landingUrl = "/login";
    window.location.href = landingUrl;
  }
}
