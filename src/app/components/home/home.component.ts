import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import * as AOS from 'aos';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {


  constructor(private router:Router) {
    //this.loadScripts();
  }

  ngOnInit(): void {
     AOS.init();
  }

  noticias(){
    /* this.router.onSameUrlNavigation = 'reload'; */
    this.router.navigate(['/dashboard/noticias']);
    /* //RECARGAR PAGINA HOME
    var landingUrl = "/dashboard/noticias";
    window.location.href = landingUrl; */

  }

  loadScripts() {
    const dynamicScripts = [
     './assets/plugins/jquery/jquery.min.js',
     './assets/plugins/bootstrap/js/bootstrap.min.js',
     './assets/js/perfect-scrollbar.jquery.min.js',
     './assets/js/waves.js',
     './assets/js/sidebarmenu.js',
     './assets/plugins/sticky-kit-master/dist/sticky-kit.min.js',
     './assets/plugins/sparkline/jquery.sparkline.min.js',
     './assets/js/custom.js',
     './assets/plugins/styleswitcher/jQuery.style.switcher.js',
     './assets/js_pagina/header.js',
     'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js',
     './assets/js_pagina/accordion.js',
     './assets/js_pagina/fondo_admin.js',
     'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js',
     './assets/js/menu.js',
     'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js',
     './assets/js/card-slider.js',
     './assets/js/loading.js',
     './assets/js/preloader.js',
     'https://maps.googleapis.com/maps/api/js?key=undefined',
     'https://www.gstatic.com/charts/loader.js',
     'https://www.google.com/jsapi',
     './assets/js/geochart.js',
     './assets/js/calendar.js',
     './assets/js/slider-home.js',
     'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v12.0',
     'https://snapwidget.com/js/snapwidget.js',
     'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v12.0'

    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

}

//prueba
