import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { RegistrosService } from '../../services/registros/registros.service';
import { Presidentes } from 'src/app/services/presidentes/presidentes.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-examinadores',
  templateUrl: './examinadores.component.html',
  styleUrls: ['./examinadores.component.scss']
})
export class ExaminadoresComponent implements OnInit {

  @Output() examinadores = new EventEmitter<string>();
  public examinador:any = [];
  public estado = '';
  public examinadorEstado:any [] = [];
  public examinadoresRegistrados:any[]=[];
  public validarRenade = false;
  public exami = {
    nombre:'',
    apellidoPaterno:'',
    apellidoMaterno:'',
    sexo:'',
    estado:'',
    gradoCN:'',
    estilo:'',
    fechaNacimiento:'',
    curp:''
  }
  public botonEnvioF = true;
  public botonEditar = false;
  public idExamninador = '';

  constructor(private _registro:RegistrosService,
              private _renade:Presidentes) { }

  ngOnInit(): void {
    this.allExaminadores();
  }

  buscarEntrenador(){
    this._renade.findRenade(this.exami.curp.toUpperCase()).subscribe((resp:any)=>{
      if (resp.ok) {
        if (resp['respuesta'].length != 0) {
          this.validarRenade = true;
          this.setEntrenadorRenade(resp['respuesta'][0])
        }else{
          Swal.fire('El atleta que trata de ingresar no cuenta con renade', '', 'error')
          this.exami.curp='';
          this.validarRenade = false
        }
      }
    })
  }

  setEntrenadorRenade(data:any){
    this.exami.nombre = data.nombre
    this.exami.apellidoPaterno = data.apellidoPaterno
    this.exami.apellidoMaterno = data.apellidoMaterno
    this.exami.sexo = data.genero
    this.exami.estado = data.estado
    this.exami.fechaNacimiento = data.fechaNacimiento
  }

  allExaminadores() {
    this._registro.obtenerExaminadores().subscribe((data: any) => {
      this.examinadorEstado = data.respuesta
      this.obtenerExaminadoresRegistrados();
    })
  }

  obtenerExaminadoresRegistrados(){
    this.estado = JSON.parse(localStorage.getItem('usuario')!)
    this.examinadorEstado.forEach((element:any)=>{
      if(element.presidente == this.estado){
        this.examinadoresRegistrados.push(element)
      }
    })
  }

  formularioExaminadores(form: NgForm){
    this.examinador.push(form.value)
    this.examinadores.emit(this.examinador)
    var dirtyFormID = 'formularioExminadores';
    var resetForm = <HTMLFormElement>document.getElementById(dirtyFormID);
    resetForm.reset();
    this.validarRenade = false
  }

  editExaminador(item:any){
    this.validarRenade = true;
    this.exami.nombre = item.nombre
    this.exami.apellidoPaterno = item.apellidoPaterno
    this.exami.apellidoMaterno = item.apellidoMaterno
    this.exami.sexo = item.sexo
    this.exami.estado = item.estado
    this.exami.fechaNacimiento = item.fechaNacimiento
    this.exami.estilo = item.estilo
    this.exami.gradoCN = item.gradoCN
    this.exami.curp = item.curp
    this.idExamninador = item._id;
    this.botonEnvioF = false;
    this.botonEditar = true;
  }
  deleteExaminador(id:any){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'ESTAS SEGURO DE ELIMINAR EL ATLETA?',
      text: "LOS CAMBIOS REALIZADOS YA NO SE PODRAN RECUPERAR",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'ELIMINAR',
      cancelButtonText: 'CANCELAR',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.examinadoresRegistrados=[];
        this._registro.eliminarExamina(id).subscribe((resp:any)=>{
            swalWithBootstrapButtons.fire(
              'ELIMINADO!',
              'EL EXAMINADOR FUE ELIMINADO CORRECTAMENTE.',
              'success'
            )
            this.allExaminadores();
        })
      }else if (result.dismiss === Swal.DismissReason.cancel){
        swalWithBootstrapButtons.fire(
          'CANCELADO',
          'LOS DATOS NO SE MODIFICARON',
          'error'
        )
      }
    })
  }
  edit(form:NgForm){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'ESTAS SEGURO DE ACTUALIZAR EL ATLETA?',
      text: "LOS CAMBIOS REALIZADOS YA NO SE PODRAN RECUPERAR",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'EDITAR',
      cancelButtonText: 'CANCELAR',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.examinadoresRegistrados=[];
        this._registro.editarExamina(this.idExamninador,form.value).subscribe((resp:any)=>{
          swalWithBootstrapButtons.fire(
            'ACTUALIZADO!',
            'EL EXAMINADOR FUE ACTUALIZADO CORRECTAMENTE.',
            'success'
          )
          this.allExaminadores();
          var dirtyFormID = 'formulario';
          var resetForm = <HTMLFormElement>document.getElementById(dirtyFormID);
          resetForm.reset();
          this.botonEnvioF = true;
          this.botonEditar = false;
          this.validarRenade = false;
          this.exami.curp = ''
        })
      }else if (result.dismiss === Swal.DismissReason.cancel){
        swalWithBootstrapButtons.fire(
          'CANCELADO',
          'LOS DATOS NO SE MODIFICARON',
          'error'
        )
      }
    })
  }

}
