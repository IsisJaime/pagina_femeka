import { Component, OnInit } from '@angular/core';
import { renades } from '../../interfaces/renade';
import { EstadosService } from '../../services/estados/estados.service';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';
import { calculateAge, fechaCurp2, curpValida, fechaCurpNuevos } from '../../functions/curpFecha';
import { RenadesService } from '../../services/renades/renades.service';
import { Presidentes } from '../../services/presidentes/presidentes.service';

import jsPDF from 'jspdf';
import 'jspdf-autotable';


@Component({
  selector: 'app-renade',
  templateUrl: './renade.component.html',
  styleUrls: ['./renade.component.scss']
})
export class RenadeComponent implements OnInit {

  public datos = {
    nombre:'',
    apellidoPaterno:'',
    apellidoMaterno:'',
    genero:'',
    curp:'',
    edad:0,
    fechaNacimiento:'',
    estado:'',
    tipo:'',
    email:'',
    telefono:'',
    club:'',
    directorClub:'',
    direccionClub:''
  }
  public edad:any;
  public fecha:any;
  public estados:any[] = [];
  public estado:any;
  public curp:any;
  public totalRenades = 0;
  public renadesResg = 0;

  constructor(private _estados:EstadosService,
              private _renade:RenadesService,
              private _presi:Presidentes) { }

  ngOnInit(): void {
    this.obtenerEstado()
    this.obtenerPresidente();
    this.getEstados();
  }

  obtenerEstado(){
    this.datos.estado = JSON.parse(localStorage.getItem('estado')!);
  }

  getEstados(){
    this.estados = this._estados.getEstados();
  }

  calcular(event:any){
    let val = curpValida(event.target.value)
    if(val){
      this.datos.fechaNacimiento = fechaCurpNuevos(event.target.value)
      this.datos.edad = calculateAge(event.target.value);
    }else{
      Swal.fire('Ingrese una curp valida', '', 'error')
      this.datos.curp = '';
    }
  }

  registrar(){
    let validar = this.validarObjetoVacio(this.datos);
    if (validar) {
      if(this.renadesResg < this.totalRenades){
        this.validarCurpExistente(this.datos.curp);
        this.obtenerEstado();
      }else{
        this.datos = {
          nombre:'',
          apellidoPaterno:'',
          apellidoMaterno:'',
          genero:'',
          curp:'',
          edad:0,
          fechaNacimiento:'',
          estado:JSON.parse(localStorage.getItem('estado')!),
          tipo:'',
          email:'',
          telefono:'',
          club:'',
          directorClub:'',
          direccionClub:''
        }
        Swal.fire('Se terminaron tus registros de renades', '', 'error');
      }
    }else{
      Swal.fire('Es necesario llenar todos los campos del renade', '', 'error')
    }
  }

  validarObjetoVacio(objeto:any){
    let contador = 0
    let validar = false
    for (var prop in objeto) {
      if(objeto[prop] !== ""){
        contador+=1
      }
    }
    if(contador == 14){
      validar = true
    }else{
      validar = false
    }
    return validar;
  }

  validarCurpExistente(curp:any){
    this._renade.getRenadeCurp(curp).subscribe((resp:any)=>{
      if (resp.ok) {
        if (resp['respuesta'].length > 0) {
          Swal.fire('La curp ya se encuentra registrada con otro renade', '', 'error');
        } else {
          let val = Object.assign({idPresidente:JSON.parse(localStorage.getItem('id')!)},this.datos)
          this._renade.agregarRenade(val).subscribe((resp:any)=>{
            if (resp.ok) {
              console.log(resp);

              let val = {
                renadesRegistrados: this.renadesResg + 1
              }
              this._presi.actualizarRenades(JSON.parse(localStorage.getItem('id')!),val).subscribe((res:any)=>{
                this.renadesResg = res['respuesta'].renadesRegistrados
              })
              this.imprimirPdf(resp['registroNewRenade']);
              this.datos = {
                nombre:'',
                apellidoPaterno:'',
                apellidoMaterno:'',
                genero:'',
                curp:'',
                edad:0,
                fechaNacimiento:'',
                estado:JSON.parse(localStorage.getItem('estado')!),
                tipo:'',
                email:'',
                telefono:'',
                club:'',
                directorClub:'',
                direccionClub:''
              }
              Swal.fire('Registro correcto en renades', '', 'success')
            }
          });
        }
      }
    })
  }

  obtenerPresidente(){
    this._presi.obtenerPresidenteEstado(JSON.parse(localStorage.getItem('id')!)).subscribe((resp:any)=>{
      if (resp.ok) {
        this.totalRenades = resp['respuesta'][0].renades
        this.renadesResg = resp['respuesta'][0].renadesRegistrados
      }
    })
  }

  imprimirPdf(datos:any){
    const doc : any = new jsPDF()
    doc.setTextColor(100);
    doc.setFont('courier')
    doc.setFontSize(8);
    var img = new Image()
    img.src = 'assets/CREDENCIAL/PNG/todo/frente.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
    doc.addImage(img, 'png', 20, 20, 100, 65)
    // var img2 = new Image()
    // img2.src = 'assets/CREDENCIAL/PNG/todo/fondo.png'
    // doc.addImage(img2, 'png', 20, 20, 100, 70)
    var img1 = new Image()
    img1.src = 'assets/CREDENCIAL/PNG/todo/fondoAtras.png'
    doc.addImage(img1, 'png', 20, 100, 100, 55)
    // var img3 = new Image()
    // img3.src = 'assets/CREDENCIAL/PNG/todo/foto1.png'
    // doc.addImage(img3, 'png', 25, 30, 100, 50)
    doc.text(datos.estado.toUpperCase(),65,45)
    doc.text("NOMBRE: "+datos.nombre.concat(" "+datos.apellidoPaterno+" "+ datos.apellidoMaterno),20,50)
    doc.text("VIGENCIA: ENERO-DICIEMBRE 2022",20,55)
    doc.text("CURP: "+datos.curp,20,60)
    doc.text("MEMBRESIA: FEMEKA",20,65)
    doc.text("FOLIO: "+datos.folio,20,70)
    doc.setTextColor(255,0,0)
    doc.text(datos.tipo,20,75)

    //  doc.output('dataurlnewwindow')
    doc.save("Credencial "+datos.nombre.concat(" "+datos.apellidoPaterno+" "+ datos.apellidoMaterno));
  }
}
