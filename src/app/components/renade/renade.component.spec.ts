import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RenadeComponent } from './renade.component';

describe('RenadeComponent', () => {
  let component: RenadeComponent;
  let fixture: ComponentFixture<RenadeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RenadeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RenadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
