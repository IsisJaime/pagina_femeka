import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { RegistrosService } from '../../services/registros/registros.service';
import { Presidentes } from 'src/app/services/presidentes/presidentes.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-juez',
  templateUrl: './juez.component.html',
  styleUrls: ['./juez.component.scss']
})
export class JuezComponent implements OnInit {

  @Output() juez = new EventEmitter<string>();
  public jueces:any = [];
  public estado = '';
  public juezEstado:any [] = [];
  public juecesRegistrados:any[]=[];
  public activarDatos = false
  public datosJuez = {
    curp:'',
    nombre:'',
    apellidoPaterno:'',
    apellidoMaterno:'',
    sexo:'',
    tipoSangre:'',
    padecimientoCronico:'',
    estado:'',
    licencia:'',
    gradoCN:'',
    estilo:'',
    fechaNacimiento:''
  }
  public botonEnviar = true;
  public botonEditar = false;
  public idJuez = '';

  constructor(private _registro:RegistrosService,
              private _renade:Presidentes) { }

  ngOnInit(): void {
    this.allJuces();
  }

  buscarEntrenador(){
    this._renade.findRenade(this.datosJuez.curp.toUpperCase()).subscribe((resp:any)=>{
      if (resp.ok) {
        if (resp['respuesta'].length != 0) {
          this.activarDatos = true;
          this.setEntrenadorRenade(resp['respuesta'][0])
        }else{
          Swal.fire('El atleta que trata de ingresar no cuenta con renade', '', 'error')
          this.datosJuez.curp='';
          this.activarDatos = false
        }
      }
    })
  }

  setEntrenadorRenade(data:any){
    this.datosJuez.nombre = data.nombre
    this.datosJuez.apellidoPaterno = data.apellidoPaterno
    this.datosJuez.apellidoMaterno = data.apellidoMaterno
    this.datosJuez.sexo = data.genero
    this.datosJuez.estado = data.estado
    this.datosJuez.fechaNacimiento = data.fechaNacimiento
  }

  allJuces() {
    this._registro.obtenerJueces().subscribe((data: any) => {
      this.juezEstado = data.respuesta;
      this.obtenerJuecesRegistrados();
    })
  }

  obtenerJuecesRegistrados(){
    this.estado = JSON.parse(localStorage.getItem('usuario')!)
    this.juezEstado.forEach((element:any)=>{
      if(element.presidente == this.estado){
        this.juecesRegistrados.push(element)
      }
    })
  }

  formularioJueces(form: NgForm){
    this.jueces.push(form.value)
    this.juez.emit(this.jueces)
    var dirtyFormID = 'formularioJuez';
    var resetForm = <HTMLFormElement>document.getElementById(dirtyFormID);
    resetForm.reset();
    this.activarDatos = false
  }

  editJuez(item:any){
    this.activarDatos = true;
    this.datosJuez.nombre = item.nombre
    this.datosJuez.apellidoPaterno = item.apellidoPaterno
    this.datosJuez.apellidoMaterno = item.apellidoMaterno
    this.datosJuez.sexo = item.sexo
    this.datosJuez.estado = item.estado
    this.datosJuez.fechaNacimiento = item.fechaNacimiento
    this.datosJuez.curp = item.curp
    this.datosJuez.estilo = item.estilo
    this.datosJuez.gradoCN = item.gradoCN
    this.datosJuez.licencia = item.licencia
    this.datosJuez.padecimientoCronico = item.padecimientoCronico
    this.datosJuez.tipoSangre = item.tipoSangre
    this.idJuez = item._id;
    this.botonEnviar = false;
    this.botonEditar = true;
  }
  deleteJuez(id:any){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'ESTAS SEGURO DE ELIMINAR EL ATLETA?',
      text: "LOS CAMBIOS REALIZADOS YA NO SE PODRAN RECUPERAR",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'ELIMINAR',
      cancelButtonText: 'CANCELAR',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.juecesRegistrados=[];
        this._registro.eliminarJueces(id).subscribe((resp:any)=>{
            swalWithBootstrapButtons.fire(
              'ELIMINADO!',
              'EL JUEZ FUE ELIMINADO CORRECTAMENTE.',
              'success'
            )
            this.allJuces();
        })
      }else if (result.dismiss === Swal.DismissReason.cancel){
        swalWithBootstrapButtons.fire(
          'CANCELADO',
          'LOS DATOS NO SE MODIFICARON',
          'error'
        )
      }
    })
  }
  edit(form:NgForm){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'ESTAS SEGURO DE ACTUALIZAR EL ATLETA?',
      text: "LOS CAMBIOS REALIZADOS YA NO SE PODRAN RECUPERAR",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'EDITAR',
      cancelButtonText: 'CANCELAR',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.juecesRegistrados=[];
        this._registro.editarJueces(this.idJuez,form.value).subscribe((resp:any)=>{
          swalWithBootstrapButtons.fire(
            'ACTUALIZADO!',
            'EL JUEZ FUE ACTUALIZADO CORRECTAMENTE.',
            'success'
          )
          this.allJuces();
          var dirtyFormID = 'formulario';
          var resetForm = <HTMLFormElement>document.getElementById(dirtyFormID);
          resetForm.reset();
          this.botonEnviar = true;
          this.botonEditar = false;
          this.activarDatos = false;
          this.datosJuez.curp = ''
        })
      }else if (result.dismiss === Swal.DismissReason.cancel){
        swalWithBootstrapButtons.fire(
          'CANCELADO',
          'LOS DATOS NO SE MODIFICARON',
          'error'
        )
      }
    })
  }

}
