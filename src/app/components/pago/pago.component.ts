import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { NgForm } from '@angular/forms';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pago',
  templateUrl: './pago.component.html',
  styleUrls: ['./pago.component.scss']
})
export class PagoComponent implements OnInit {
  @Output() pagos = new EventEmitter<any[]>();

  constructor() { }

  ngOnInit(): void {
  }

  files: File[] = [];

  onSelect(event:any) {
    if(this.files.length != 0){
      Swal.fire({
        icon: 'error',
        title: 'Lo sentimos',
        text: 'No puedes subir mas de una imagen',
      })
    }else{
      this.files.push(...event.addedFiles);
      this.files.forEach(element => {
        if(element.type != "image/jpeg"){
          this.onRemove(element);
          Swal.fire({
            icon: 'error',
            title: 'Lo sentimos',
            text: 'El archivo no es una imagen',
          })
        }
      });
      this.pagos.emit(this.files)
    }

  }

  onRemove(event:any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }
}
