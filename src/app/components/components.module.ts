import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AtletaComponent } from './atleta/atleta.component';
import { ExaminadoresComponent } from './examinadores/examinadores.component';
import { JuezComponent } from './juez/juez.component';
import { EntrenadoresComponent } from './entrenadores/entrenadores.component';
import { PagoComponent } from './pago/pago.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { FooterComponent } from './footer/footer.component';
import { HeaderPublicoComponent } from './header-publico/header-publico.component';
import { GalleryComponent } from './gallery/gallery.component';
import { PreloaderComponent } from './preloader/preloader.component';
import { CalendarComponent } from './calendar/calendar.component';
import { BannerCampeonatoComponent } from './banner-campeonato/banner-campeonato.component';
import { MenuExtendidoComponent } from './menu-extendido/menu-extendido.component';
import { HomeComponent } from './home/home.component';
import { MenuExtendidoAdminComponent } from './menu-extendido-admin/menu-extendido-admin.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { RenadeComponent } from './renade/renade.component';
import { MenuSuperAdminComponent } from './menu-super-admin/menu-super-admin.component';
import { EquipoAtletasComponent } from './equipo-atletas/equipo-atletas.component';
@NgModule({
  declarations: [
    ExaminadoresComponent,
    JuezComponent,
    EntrenadoresComponent,
    PagoComponent,
    AtletaComponent,
    FooterComponent,
    HeaderPublicoComponent,
    GalleryComponent,
    PreloaderComponent,
    CalendarComponent,
    BannerCampeonatoComponent,
    MenuExtendidoComponent,
    HomeComponent,
    MenuExtendidoAdminComponent,
    RenadeComponent,
    MenuSuperAdminComponent,
    EquipoAtletasComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    RouterModule,
    NgxDropzoneModule,
    NgxSpinnerModule
  ],
  exports:[
    ExaminadoresComponent,
    JuezComponent,
    EntrenadoresComponent,
    PagoComponent,
    AtletaComponent,
    HeaderPublicoComponent,
    GalleryComponent,
    FooterComponent,
    PreloaderComponent,
    CalendarComponent,
    BannerCampeonatoComponent,
    MenuExtendidoComponent,
    HomeComponent,
    MenuExtendidoAdminComponent,
    RenadeComponent,
    MenuSuperAdminComponent,
    EquipoAtletasComponent
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class ComponentsModule { }
