import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerCampeonatoComponent } from './banner-campeonato.component';

describe('BannerCampeonatoComponent', () => {
  let component: BannerCampeonatoComponent;
  let fixture: ComponentFixture<BannerCampeonatoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BannerCampeonatoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerCampeonatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

