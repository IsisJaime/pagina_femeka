import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuExtendidoComponent } from './menu-extendido.component';

describe('MenuExtendidoComponent', () => {
  let component: MenuExtendidoComponent;
  let fixture: ComponentFixture<MenuExtendidoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuExtendidoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuExtendidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
