export interface renades {
  estado: '',
  nombre: '',
  apellidoPaterno:'',
  apellidoMaterno:'',
  fechaNacimiento:'',
  edad:0,
  vigencia:'',
  curp:'',
  tipo:'',
  club:'',
  directorClub:'',
  direccionClub:'',
  email:'',
  genero:'',
  telefono:''
}
