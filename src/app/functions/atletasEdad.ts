export function atletasFilter (atletas:any, edad:string){
  let atletasFilt:any[] = [];
  atletas.forEach((element:any) => {
    if ( parseInt(edad) >= element.edadMin && parseInt(edad) <= element.edadMax) {
      atletasFilt.push(element);
    }
  });
  return atletasFilt;
}

export function modalidad (modalidad:any){
  let valor = {
    modalidad:modalidad.split(" ")[0],
    integrantes:'EQUIPO'
  }
  return valor;
}
