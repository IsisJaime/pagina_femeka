export function fechaCurp(curp:any){
  let separar = curp.slice(4,-8)
  separar = separar.split('')
  let año = separar[0].concat(separar[1])
  let mes = separar[2].concat(separar[3])
  let dia = separar[4].concat(separar[5])
  let fecha = dia.concat('/'+ mes).concat('/'+ año)
  return fecha;
}

export function calculateAge(curp:any) {
  var fecha = fechaCurpNuevos(curp);
  return calcularEdad(fecha)
}

function calcularEdad(fecha:any) {
  var hoy = new Date();
  var cumpleanos = new Date(fecha);
  var edad = hoy.getFullYear() - cumpleanos.getFullYear();
  var m = hoy.getMonth() - cumpleanos.getMonth();
  if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
      edad--;
  }
  return edad;
}

export function fechaCurp2(curp:any){
  let separar = curp.slice(4,-8)
  separar = separar.split('')
  let comple = '19'
  let año = separar[0].concat(separar[1])
  let mes = separar[2].concat(separar[3])
  let dia = separar[4].concat(separar[5])
  let fecha = comple.concat(año).concat('-'+ mes).concat('-'+ dia)
  console.log(fecha);

  return fecha;
}

export function fechaCurpNuevos(curpRenade:any){
  let separar = curpRenade.slice(4,-8)
  separar = separar.split('');
  let año = separar[0].concat(separar[1])
  let mes = separar[2].concat(separar[3])
  let dia = separar[4].concat(separar[5])
  let fecha = año.concat(mes).concat(dia)
  var curp = fecha.toString();
  var currentYearCentury = parseInt(new Date().getFullYear().toString().substr(0,2) + '00');
  var lastYearCentury = (currentYearCentury - 100);
  var yearCURP = parseInt(curp.substr(0, 2));
  var monthCURP = parseInt(curp.substr(2, 2)) - 1;
  var dayCURP = parseInt(curp.substr(4, 2));
  var birthdate = null;

  if (monthCURP >= 0 && monthCURP <= 11 && dayCURP >= 1 && dayCURP <= 31) {
      if ((currentYearCentury + yearCURP) > new Date().getFullYear()) {
          birthdate = new Date((lastYearCentury + yearCURP), monthCURP, dayCURP, 0, 0, 0, 0);
      } else {
          birthdate = new Date((currentYearCentury + yearCURP), monthCURP, dayCURP, 0, 0, 0, 0);
      }
  }

  if (birthdate != null) {
      var yearsDifference = (new Date().getTime() - birthdate.getTime()) / 1000;
      yearsDifference /= (60 * 60 * 24);
      Math.abs(Math.round(yearsDifference/365.25));
  }

  return birthdate!.toISOString().split('T')[0];

}

export function curpValida(curp:any) {
  var re = /^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/,
      validado = curp.match(re);

  if (!validado)
    return false;

  function digitoVerificador(curp17:any) {
      var diccionario  = "0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZ",
          lngSuma      = 0.0,
          lngDigito    = 0.0;
      for(var i=0; i<17; i++)
          lngSuma = lngSuma + diccionario.indexOf(curp17.charAt(i)) * (18 - i);
      lngDigito = 10 - lngSuma % 10;
      if (lngDigito == 10) return 0;
      return lngDigito;
  }

  if (validado[2] != digitoVerificador(validado[1]))
    return false;

  return true;
}
