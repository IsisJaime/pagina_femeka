import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nopagefound',
  templateUrl: './nopagefound.component.html',
  styleUrls: ['./nopagefound.component.scss']
})
export class NopagefoundComponent implements OnInit{

  constructor(private router: Router) {}

  ngOnInit(): void {
  }

  year = new Date().getFullYear();

  redireccion(){
    this.router.navigate(['/#/dashboard'])
  }
}
