google.load('visualization', '1', { 'packages': ['geochart'] });
google.setOnLoadCallback(drawVisualization);

function drawVisualization() {
  var data = new google.visualization.DataTable();

  data.addColumn('string', 'Country');
  data.addColumn('number', 'Value');
  data.addColumn({ type: 'string', role: 'tooltip' }); var ivalue = new Array();

  data.addRows([[{ v: 'MX-AGU', f: 'Aguascalientes' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-AGU'] = '<img style="width: 20%;  float:right;"><h2>Asociación de Karate y Artes Marciales Afines del Estado de Aguascalientes A.C.</h2> ';

  data.addRows([[{ v: 'MX-BCN', f: 'Baja California' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-BCN'] = '<img style="width: 20%;  float:right;"><h2>Asociación Estatal de Karate Do y Artes Marciales Afines de B. Cfa. Nte. A.C.</h2> <h3><strong>PRESIDENTE: </strong>ISMAEL CASTILLO ORTIZ</h3><h3><strong>TELEFONO: </strong>686 203 9726</h3><h3><strong>EMAIL: </strong>ing.icastillo81@hotmail.com</h3> ';

  data.addRows([[{ v: 'MX-BCS', f: 'Baja California Sur' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-BCS'] = '<img style="width: 20%;  float:right;" src="../../../assets/images/img_pagina/LOGOS/BAJASUR.jpg"><h2>Asociación de Karate de Baja California Sur, A.C.</h2> <h3><strong>PRESIDENTE: </strong>MIGUEL LOMAS GARCIA</h3><h3><strong>TELEFONO: </strong>624 125 7078</h3><h3><strong>EMAIL: </strong>karatels81@gmail.com</h3> ';

  data.addRows([[{ v: 'MX-CAM', f: 'Campeche' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-CAM'] = '<img style="width: 20%;  float:right;"><h2>Asociación de Karate Do del Estado de Campeche</h2> <h3><strong>PRESIDENTE: </strong>VICTOR ORAMAS GUZMAN</h3><h3><strong>TELEFONO: </strong>938 157 6394</h3><h3><strong>EMAIL: </strong>oramas814@hotmail.com</h3> ';

  data.addRows([[{ v: 'MX-CHP', f: 'Chiapas' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-CHP'] = '<img style="width: 20%;  float:right;"><h2>Enlace Femeka Instituto del Deporte de Chiapas</h2> <h3><strong>ENLACE: </strong>GILBERTO HERNÁNDEZ MAZA</h3><h3><strong>TELEFONO: </strong>9612330174</h3><h3><strong>EMAIL: </strong>shotogilhm@hotmail.com</h3> ';

  data.addRows([[{ v: 'MX-CHH', f: 'Chihuaha' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-CHH'] = '<img style="width: 20%;  float:right;" src="../../../assets/images/img_pagina/LOGOS/CHIHUAHUA.jpg"><h2>Asociación de Karate del Estado de Chihuahua</h2> <h3><strong>PRESIDENTE: </strong>JESUS MANUEL MONTOYA LUNA</h3><h3><strong>TELEFONO: </strong>614 176 5998</h3><h3><strong>EMAIL: </strong>akech1720@yahoo.com</h3> ';

  data.addRows([[{ v: 'MX-COA', f: 'Coahuila' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-COA'] = '<img style="width: 20%;  float:right;" src="../../../assets/images/img_pagina/LOGOS/COAHUILA.jpg"><h2>Asociación de Karate Do del Estado de Coahuila A.C.</h2> <h3><strong>ENLACE: </strong>MTRO. NESTOR ALEJANDRO ALFARO</h3><h3><strong>TELEFONO: </strong>844 127 5498</h3><h3><strong>EMAIL: </strong>n_alejandro76@hotmail.com</h3> ';

  data.addRows([[{ v: 'MX-COL', f: 'Colima' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-COL'] = '<img style="width: 20%;  float:right;"><h2>Asociación de Karate Do del Estado de Colima A.C.</h2> <h3><strong>PRESIDENTE: </strong>JESUS MANCILLA CHAVEZ</h3><h3><strong>TELEFONO: </strong>312 124 4095</h3><h3><strong>EMAIL: </strong>kumite73@yahoo.com.mx</h3> ';

  data.addRows([[{ v: 'MX-DIF', f: 'Ciudad de México' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-DIF'] = '<img style="width: 20%;  float:right;"><h2>Asociación de Karate Do y Artes Marciales Afines de la Ciudad de México A.C.</h2> <h3><strong>ENLACE: </strong>WALDO RAMÍREZ FLORES</h3><h3><strong>TELEFONO: </strong>55 5405 5623</h3><h3><strong>EMAIL: </strong>rzwaldo@hotmail.com</h3>';

  data.addRows([[{ v: 'MX-DUR', f: 'Durango' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-DUR'] = '<img style="width: 20%;  float:right;"><h2>Asociación de Karate de Durango, A.C.</h2>';

  data.addRows([[{ v: 'MX-MEX', f: 'EDO. MEX.' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-MEX'] = '<img style="width: 20%;  float:right;" src="../../../assets/images/img_pagina/LOGOS/ESTADOMEXICO.jpg"><h2>Asociación de Karate del Estado de México, A.C.</h2> <h3><strong>PRESIDENTE: </strong>MIGUIEL ANGEL HERNANDEZ GARCIA</h3><h3><strong>TELEFONO: </strong>55 5407 8313</h3><h3><strong>EMAIL: </strong>akemakiwara@yahoo.com.mx</h3> ';

  data.addRows([[{ v: 'MX-GUA', f: 'Guanajuato' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-GUA'] = '<img style="width: 20%;  float:right;"><h2>Asociación Guanajuatense de Karate Do y Artes Marciales Afines A.C.</h2>';

  data.addRows([[{ v: 'MX-GRO', f: 'Guerrero' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-GRO'] = '<img style="width: 20%;  float:right;" src="../../../assets/images/img_pagina/LOGOS/GUERRERO.jpg"><h2>Asociación Guerrerense de Karate Do y Artes Marciales Afines, A.C.</h2> <h3><strong>PRESIDENTE: </strong>ERNESTO GUZMAN HERNANDEZ</h3><h3><strong>TELEFONO: </strong>747 117 3422</h3><h3><strong>EMAIL: </strong>erguz_07@yahoo.com.mx</h3> ';

  data.addRows([[{ v: 'MX-HID', f: 'Hidalgo' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-HID'] = '<img style="width: 20%;  float:right;" src="../../../assets/images/img_pagina/LOGOS/HIDALGO.png"><h2>Asociación Hidalguense de Karate, A.C.</h2> <h3><strong>PRESIDENTE: </strong>PEDRO VERA FRANCO</h3><h3><strong>TELEFONO: </strong>779 101 6692</h3><h3><strong>EMAIL: </strong>pedrovera812@gmail.com</h3> ';

  data.addRows([[{ v: 'MX-JAL', f: 'Jalisco' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-JAL'] = '<img style="width: 20%;  float:right;" src="../../../assets/images/img_pagina/LOGOS/JALISCO.jpg"><h2>Asociación de Jalisco de Karate Tradicional, Asociación Civil</h2> <h3><strong>PRESIDENTE: </strong>MARTHA EVA SALAZAR FLORES</h3><h3><strong>TELEFONO: </strong>33 1989 6227</h3><h3><strong>EMAIL: </strong>maed_0105@hotmail.com</h3> ';

  data.addRows([[{ v: 'MX-MIC', f: 'Michoacán' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-MIC'] = '<img style="width: 20%;  float:right;" src="../../../assets/images/img_pagina/LOGOS/MICHOACAN.png"><h2>Asociación de Karate de Michoacán, A.C.</h2> <h3><strong>PRESIDENTE: </strong>HUGO BAROJAS</h3><h3><strong>TELEFONO: </strong>443 214 5912</h3><h3><strong>EMAIL: </strong>cebracu72@hotmail.com</h3> ';

  data.addRows([[{ v: 'MX-MOR', f: 'Morelos' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-MOR'] = '<img style="width: 20%;  float:right;" src="../../../assets/images/img_pagina/LOGOS/MORELOS.jpg"><h2>Karatecas Asociados del Estado de Morelos, A.C.</h2> <h3><strong>PRESIDENTE: </strong>YANKEL GODINEZ PEREZ</h3><h3><strong>TELEFONO: </strong>777 107 4818</h3><h3><strong>EMAIL: </strong>karatecasasociados.morelos@gmail.com</h3> ';

  data.addRows([[{ v: 'MX-NAY', f: 'Nayarit' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-NAY'] = '<img style="width: 20%;  float:right;"><h2>Asociación de Karate y Artes Marciales Afines del Estado de Nayarit A.C.</h2> <h3><strong>PRESIDENTE: </strong>JOSÉ ISRAEL RUELAS CARRILLO</h3><h3><strong>TELEFONO: </strong>311 338 5110</h3><h3><strong>EMAIL: </strong>israel.ruelas.carrillo@gmail.com</h3> ';

  data.addRows([[{ v: 'MX-NLE', f: 'Nuevo León' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-NLE'] = '<img style="width: 20%;  float:right;"><h2>Asociación Oficial de Karate y Artes Marciales Afines del Estado de Nuevo León A.C.</h2> <h3><strong>PRESIDENTE: </strong>JESÚS ROBERTO RODRÍGUEZ TAMEZ</h3><h3><strong>TELEFONO: </strong>81 8018 0291</h3><h3><strong>EMAIL: </strong>churdz@hotmail.com</h3> ';

  data.addRows([[{ v: 'MX-OAX', f: 'Oaxaca' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-OAX'] = '<img style="width: 20%;  float:right;" src="../../../assets/images/img_pagina/LOGOS/OAXACA.jpg"><h2>Asociación Oaxaqueña de Karate Do y Afines A.C.</h2> <h3><strong>PRESIDENTE: </strong>JORGE EDUARDO MEZA LOPEZ</h3><h3><strong>TELEFONO: </strong>951 212 8074</h3><h3><strong>EMAIL: </strong>asoc.oax.karatedo@outlook.com</h3> ';

  data.addRows([[{ v: 'MX-PUE', f: 'Puebla' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-PUE'] = '<img style="width: 20%;  float:right;" src="../../../assets/images/img_pagina/LOGOS/PUEBLA.jpeg"><h2>Karate Do y Artes Marciales de Puebla, A.C.</h2> <h3><strong>PRESIDENTE: </strong>ALFONSO AGUIRRE GONZALEZ</h3><h3><strong>TELEFONO: </strong>222 159 7924</h3><h3><strong>EMAIL: </strong>kamp.puebla@gmail.com</h3> ';

  data.addRows([[{ v: 'MX-QUE', f: 'Querétaro' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-QUE'] = '<img style="width: 20%;  float:right;"><h2>Asociación Estatal de Karate Do del Estado de Querétaro A.C.</h2><h3><strong>ENLACE: </strong>DAVID MELGOZA CASTILLO </h3><h3><strong>TELEFONO: </strong>55 2107 1077</h3><h3><strong>EMAIL: </strong>queretarofemekaoficial@gmail.com</h3>';

  data.addRows([[{ v: 'MX-ROO', f: 'Quintana Roo' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-ROO'] = '<img style="width: 20%;  float:right;" src="../../../assets/images/img_pagina/LOGOS/QUINTANA.jpg"><h2>Asociación de Karate Do del Estado de Quintana Roo, A.C.</h2> <h3><strong>PRESIDENTE: </strong>DOMINGO GONZALEZ PALAVICINI</h3><h3><strong>TELEFONO: </strong>998 167 6773</h3><h3><strong>EMAIL: </strong>palavicinicancun@yahoo.com.mx</h3> ';

  data.addRows([[{ v: 'MX-SLP', f: 'San Luis Potosí' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-SLP'] = '<img style="width: 20%;  float:right;" src="../../../assets/images/img_pagina/LOGOS/SAN LUIS POTOSI.jpg"><h2>Asociación Potosina de Karate y Artes Marciales Afines, A.C.</h2> <h3><strong>PRESIDENTE: </strong>JULIAN MARTINEZ SUAREZ</h3><h3><strong>TELEFONO: </strong>444 169 5071</h3><h3><strong>EMAIL: </strong>shitoryuslp@hotmail.com</h3> ';

  data.addRows([[{ v: 'MX-SIN', f: 'Sinaloa' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-SIN'] = '<img style="width: 20%;  float:right;"><h2>Asociación Estatal de Karate Do y Artes Marciales Afines de Sinaloa A.C.</h2> <h3><strong>ENLACE: </strong>JUAN DE DIOS</h3><h3><strong>TELEFONO: </strong>667 451 0457</h3><h3><strong>EMAIL: </strong>zaya_88@hotmail.com</h3> ';

  data.addRows([[{ v: 'MX-SON', f: 'Sonora' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-SON'] = '<img style="width: 20%;  float:right;"><h2>Asociación Sonorense de Karate Do y Artes Marciales Afines, A.C.</h2> <h3><strong>ENLACE: </strong>CARLOS VALENZUELA</h3><h3><strong>TELEFONO: </strong>642 426 3729</h3><h3><strong>EMAIL: </strong>gaston.quintero.kai@gmail.com</h3> ';

  data.addRows([[{ v: 'MX-TAB', f: 'Tabasco' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-TAB'] = '<img style="width: 20%;  float:right;"><h2>Asociación Tabasqueña de Karate Do y Afines, A.C.</h2> <h3><strong>PRESIDENTE: </strong>SAMUEL DE LA CRUZ COY</h3><h3><strong>TELEFONO: </strong>993 125 6677</h3><h3><strong>EMAIL: </strong>atktabasco@hotmail.com</h3> ';

  data.addRows([[{ v: 'MX-TAM', f: 'Tamaulipas' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-TAM'] = '<img style="width: 20%;  float:right;" src="../../../assets/images/img_pagina/LOGOS/TAMAULIPAS.jpg"><h2>Asociación de Karate de Tamaulipas A.C.</h2> <h3><strong>PRESIDENTE: </strong>Miguel Ángel Maciel Romero</h3><h3><strong>TELEFONO: </strong>899 963 7864</h3><h3><strong>EMAIL: </strong>miguel.angel.macielr@hotmail.com</h3> ';

  data.addRows([[{ v: 'MX-TLA', f: 'Tlaxcala' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-TLA'] = '<img style="width: 20%;  float:right;"><h2>Asociación de Karate y Artes Marciales Afines del Estado de Tlaxcala, A.C.</h2> <h3><strong>PRESIDENTE: </strong>ROY PEREZ SERRANO</h3><h3><strong>TELEFONO: </strong>246 172 1817</h3><h3><strong>EMAIL: </strong>roy.perezserrano@gmail.com</h3> ';

  data.addRows([[{ v: 'MX-VER', f: 'Veracruz' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-VER'] = '<img style="width: 20%;  float:right;" src="../../../assets/images/img_pagina/LOGOS/VERACRUZ.jpg"><h2>Asociación Veracruzana de Karate Do, A.C.</h2> <h3><strong>ENLACE: </strong>JESUS MUÑOZ BARRADAS </h3';

  data.addRows([[{ v: 'MX-YUC', f: 'Yucatán' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-YUC'] = '<img style="width: 20%;  float:right;" src="../../../assets/images/img_pagina/LOGOS/YUCATAN.png"><h2>Asociación Yucateca de Karate Do A.C.</h2> <h3><strong>PRESIDENTE: </strong>MAURO F.  MOGUEL ESPERON</h3><h3><strong>TELEFONO: </strong>999 900 8448</h3><h3><strong>EMAIL: </strong>kbri07@hotmail.com</h3> ';

  data.addRows([[{ v: 'MX-ZAC', f: 'Zacatecas' }, 0, 'Haz click para saber más de la asociación']]);
  ivalue['MX-ZAC'] = '<img style="width: 20%;  float:right;"><h2>Karate Asociado de Zacatecas, A. C.</h2> <h3><strong>PRESIDENTE: </strong>GUILLERMO BECERRRA</h3><h3><strong>TELEFONO: </strong>494 103 6476</h3><h3><strong>EMAIL: </strong>shito-ryu-mazac@hotmail.com</h3> ';


  var options = {
    colorAxis: { minValue: 0, maxValue: 49, colors: ['#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#438094', '#DE3403', '#DE3403', '#DE3403', '#DE3403', '#DE3403', '#DE3403', '#DE3403', '#DE3403', '#DE3403', '#DE3403', '#DE3403', '#DE3403', '#DE3403', '#DE3403', '#DE3403', '#DE3403', '#E0D39E', '#E0D39E', '#E0D39E', '#E0D39E', '#E0D39E', '#E0D39E', '#E0D39E', '#E0D39E',] },
    legend: 'none',
    backgroundColor: 'transparent',
    datalessRegionColor: 'transparent',
    displayMode: 'regions',
    enableRegionInteractivity: 'true',
    resolution: 'provinces',
    sizeAxis: { minValue: 1, maxValue: 1, minSize: 10, maxSize: 10 },
    region: 'MX',
    keepAspectRatio: true,
    tooltip: { textStyle: { color: '#444444' }, trigger: 'hover' }
  };
  var chart = new google.visualization.GeoChart(document.getElementById('map'));
	  google.visualization.events.addListener(chart, 'select', function() {
	  var selection = chart.getSelection();
	  if (selection.length == 1) {
	  var selectedRow = selection[0].row;
	  var selectedRegion = data.getValue(selectedRow, 0);
	  if(ivalue[selectedRegion] != '') { document.getElementById('message').innerHTML = ivalue[selectedRegion]; }
	  }
	  });
	 chart.draw(data, options);
}


