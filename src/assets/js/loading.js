$(document).ready(function() {
  // Aqui se cargo el DOM, osea la estructura de HTML, etiquetas, etc
});

$(window).on('load', function() {
  // Aquí se solicitaron las imágenes
  // Se podría decir que la página esta lista para ser mostrada
  // Por lo mismo removemos el loading que teníamos
  
  setTimeout(function() { // eliminar este línea de código
    
    $('.loading').fadeOut('slow', function(){ // Aquí oculto la capa loading
      $(this).remove(); // y aqui la elimino una vez que la oculte
    });
    
  }, 1000); // y esta también
  
});


// Esas dos lineas que comento que se tienen que eliminar son un delay que pongo para que se alcance a ver el loading, hay que eliminarlas