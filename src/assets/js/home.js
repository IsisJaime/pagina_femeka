const items = document.querySelectorAll('[data-src]')
const popin = document.querySelector('.popin')
const closeBtn = document.querySelector('.popin-btn-close')


if (items && popin && closeBtn) {
  closeBtn.addEventListener('click', e => {
    const el = popin.querySelector('img')
    el.remove()
    popin.style.display = 'none'
  })
  items.forEach(i => {
    i.addEventListener('click', el => {
      const picture = el.target.src
      const img = document.createElement('img')
      img.setAttribute('src', picture)
      popin.appendChild(img)
      popin.style.display = 'flex'
    })
  })
}