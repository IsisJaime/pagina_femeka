console.clear();
const tabs = Array.from(document.querySelectorAll('[data-template="tabs"]'));		
tabs.forEach(tab => {
	const titles = Array.from(tab.querySelectorAll('.tabs-navigation > *:not(.tabs-navigation_label)'));
	const underline = tab.querySelector('.tabs-navigation_labelk');
	const tab_content_items = tab.querySelectorAll('.tabs-content > *');
	
	
	const _requestLabelPosition = (selector) => {
		let _requestTabID = selector.getAttribute('data-tab');
		
		let cssData = {
			width: window.getComputedStyle(selector, null).width,
			offset: selector.offsetLeft + 'px'
		}
		underline.style.width = cssData.width;
		underline.style.transform = 'translatex(' + cssData.offset + ')';
		
		tab_content_items.forEach(content_item => {
			if(content_item.id !== _requestTabID) {
				content_item.classList.remove('active');
			} else {
				content_item.classList.add('active');
			}			
		})
	}
	
	titles.forEach((title, i) => {
		
		//Initial state of the underline label
		if(title.classList.contains('active')) _requestLabelPosition(title)

		title.addEventListener('click', () => {
			_requestLabelPosition(title)
			for(let item in titles) {
				titles[item].classList.remove('active');
			}
			title.classList.add('active');
			
		})
	})
})



